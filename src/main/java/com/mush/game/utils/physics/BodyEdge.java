/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.physics;

/**
 *
 * @author mush
 */
public interface BodyEdge {

    /**
     *
     * @param body
     * @param dx normalized distance vector.x
     * @param dy normalized distance vector.y
     * @return
     */
    public double getDistance(FreeBody body, double dx, double dy);

    public double getWidth();

    public double getHeight();
}
