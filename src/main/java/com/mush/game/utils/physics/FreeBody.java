/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.physics;

import java.awt.geom.Point2D;

/**
 *
 * @author mush
 */
public class FreeBody {

    private static final double TAU = Math.PI * 2;

    private double mass;
    private double angMass;
    public double xPosition;
    public double yPosition;
    public double zRotation;
    public double xVelocity;
    public double yVelocity;
    public double zAngVelocity;
    public double xForce;
    public double yForce;
    public double zTorque;

    private double sin;
    private double cos;

    public double dragFactor = 0;
    public double maxSpeed = 1000;
    public double maxAngVelocity = TAU * 1.5;

    public FreeBody(double mass) {
        this(mass, 1);
    }

    public FreeBody(double mass, double angMassDistribution) {
        setMass(mass, angMassDistribution);
        reset();
    }

    public void setMass(double mass, double angMassDistribution) {
        this.mass = mass;
        this.angMass = mass * angMassDistribution;
    }

    public void setDragFactor(double factor) {
        dragFactor = factor * mass;
    }

    public void setZRotation(double rot) {
        zRotation = rot;
        calculateSinCos();
    }

    public void reset() {
        xPosition = 0;
        yPosition = 0;
        zRotation = 0;
        calculateSinCos();
        stop();
    }

    private void calculateSinCos() {
        sin = Math.sin(zRotation);
        cos = Math.cos(zRotation);
    }

    public void stop() {
        xVelocity = 0;
        yVelocity = 0;
        zAngVelocity = 0;
        clearForces();
    }

    public double distanceTo(FreeBody body) {
        double dx = this.xPosition - body.xPosition;
        double dy = this.yPosition - body.yPosition;
        return Math.sqrt(dx * dx + dy * dy);
    }

    public double getGlobalToLocalX(double globalX, double globalY) {
        return globalX * cos + globalY * sin;
    }

    public double getGlobalToLocalY(double globalX, double globalY) {
        return globalY * cos - globalX * sin;
    }

    public double getLocalToGlobalX(double localX, double localY) {
        return cos * localX - sin * localY;
    }

    public double getLocalToGlobalY(double localX, double localY) {
        return cos * localY + sin * localX;
    }

    public void applyGlobalForce(double x, double y) {
        xForce += x;
        yForce += y;
    }

    public void applyLocalForce(double x, double y) {
        xForce += getLocalToGlobalX(x, y);
        yForce += getLocalToGlobalY(x, y);
    }

    public void getLocalVelocity(Point2D.Double result) {
        result.x = getGlobalToLocalX(xVelocity, yVelocity);
        result.y = getGlobalToLocalY(xVelocity, yVelocity);
    }

    public void setLocalVelocity(double x, double y) {
        xVelocity = getLocalToGlobalX(x, y);
        yVelocity = getLocalToGlobalY(x, y);
    }

    public void addLocalVelocity(double x, double y) {
        xVelocity += getLocalToGlobalX(x, y);
        yVelocity += getLocalToGlobalY(x, y);
    }

    public void applyTorque(double z) {
        zTorque += z;
    }

    public void clearForces() {
        xForce = 0;
        yForce = 0;
        zTorque = 0;
    }

    public void update(double elapsedSeconds) {
        // s = v * t;
        // v +=  a * t
        // a = f / m

        xPosition += xVelocity * elapsedSeconds;
        yPosition += yVelocity * elapsedSeconds;
        zRotation += zAngVelocity * elapsedSeconds;

        if (zRotation > TAU) {
            zRotation %= TAU;
        } else if (zRotation < -TAU) {
            zRotation %= -TAU;
        }

        if (zAngVelocity != 0) {
            // zRotation has changed
            double absAngVel = Math.abs(zAngVelocity);
            if (absAngVel > maxAngVelocity) {
                zAngVelocity = maxAngVelocity * Math.signum(zAngVelocity);
            }

            calculateSinCos();
        }

        // only do acc if non-zero mass
        if (mass != 0) {
            double xAcc = getAccForForce(xForce);
            double yAcc = getAccForForce(yForce);
            if (dragFactor != 0) {
                xAcc -= getAccForForce(xVelocity * dragFactor);
                yAcc -= getAccForForce(yVelocity * dragFactor);
            }
            xVelocity += xAcc * elapsedSeconds;
            yVelocity += yAcc * elapsedSeconds;

            double speed = getSpeed();
            if (speed > maxSpeed) {
                double fSpeed = maxSpeed / speed;
                xVelocity *= fSpeed;
                yVelocity *= fSpeed;
            }
        }
        if (angMass != 0) {
            double zAngAcc = getAngAccForTorque(zTorque);
            zAngVelocity += zAngAcc * elapsedSeconds;
        }
    }

    public double getForceForAcc(double acc) {
        return acc * mass;
    }

    public double getAccForForce(double force) {
        return mass == 0
                ? 0
                : force / mass;
    }

    public double getAngAccForTorque(double torque) {
        return angMass == 0
                ? 0
                : torque / angMass;
    }

    public double getMass() {
        return mass;
    }

    public double getAngMass() {
        return angMass;
    }

    public double getSpeed() {
        return Math.sqrt(xVelocity * xVelocity + yVelocity * yVelocity);
    }

}
