/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.physics;

/**
 *
 * @author mush
 */
public class BodyCollision {

    public static boolean isCoarseCollision(FreeBody bodyA, FreeBody bodyB, double range) {
        double dx = Math.abs(bodyB.xPosition - bodyA.xPosition);
        double dy = Math.abs(bodyB.yPosition - bodyA.yPosition);

        return dx < range && dy < range;
    }

    public static double collide(FreeBody bodyA, FreeBody bodyB, BodyEdge edgeA, BodyEdge edgeB, double elasticity) {
        if (edgeA == null && edgeB == null) {
            // Never a collision between point bodies
            return 0;
        }

        // Center distance vector
        double dx = bodyB.xPosition - bodyA.xPosition;
        double dy = bodyB.yPosition - bodyA.yPosition;
        double d = Math.sqrt(dx * dx + dy * dy);

        if (d == 0) {
            return 0;
        }

        // Normalize center distance vector
        dx /= d;
        dy /= d;

        double edgeDistanceA = edgeA != null
                ? edgeA.getDistance(bodyA, dx, dy)
                : 0;

        double edgeDistanceB = edgeB != null
                ? edgeB.getDistance(bodyB, -dx, -dy)
                : 0;

        if (d > edgeDistanceA + edgeDistanceB) {
            return 0;
        }

        // Relative velocity
        double relVx = bodyA.xVelocity - bodyB.xVelocity;
        double relVy = bodyA.yVelocity - bodyB.yVelocity;

        // Project relative velocity to normalized distance vector
        double relVProject = relVx * dx + relVy * dy;

        // Projection must be positive, rel velocity towards and not away from B
        if (relVProject < 0) {
            return 0;
        }

        // Projected velocities to the normalized distance vector
        double aVProject = (bodyA.xVelocity * dx + bodyA.yVelocity * dy) * elasticity;
        double bVProject = (bodyB.xVelocity * dx + bodyB.yVelocity * dy) * elasticity;

        // Projected values applied to normalized distance vector
        double projAx = aVProject * dx;
        double projAy = aVProject * dy;

        double projBx = bVProject * dx;
        double projBy = bVProject * dy;

        // v1 = u1 * (m1 - m2) / (m1 + m2) + u2 * (2 * m2) / (m1 + m2)
        // v2 = u1 * (2m1) / (m1 + m2) + u2 * (m2 - m1) / (m1 + m2)
        double mBoth = bodyA.getMass() + bodyB.getMass();
        if (mBoth == 0) {
            return 0;
        }
        double mAminB = (bodyA.getMass() - bodyB.getMass()) / mBoth;
        double mBminA = (bodyB.getMass() - bodyA.getMass()) / mBoth;
        double m2A = (bodyA.getMass() * 2) / mBoth;
        double m2B = (bodyB.getMass() * 2) / mBoth;

        bodyA.xVelocity += projAx * mAminB + projBx * m2B - projAx;
        bodyA.yVelocity += projAy * mAminB + projBy * m2B - projAy;

        bodyB.xVelocity += projAx * m2A + projBx * mBminA - projBx;
        bodyB.yVelocity += projAy * m2A + projBy * mBminA - projBy;

        // Vector perpendicular to normalized distance vector
        double perpDx = dy;
        double perpDy = -dx;

        // Project relative velocity to perpenciular distance vector
        double relPerpProject = relVx * perpDx + relVy * perpDy;

        // Relative rotation (added instead of subtracted because 180 tangents)
        double relZVelocity = bodyA.zAngVelocity + bodyB.zAngVelocity;
        // Rotation from perpendicular relative velocity
        double relZVelFromPerpVel = relPerpProject / d;

        double amBoth = bodyA.getAngMass() + bodyB.getAngMass();
        if (amBoth == 0) {
            return relVProject;
        }

        bodyA.zAngVelocity += (relZVelFromPerpVel - relZVelocity / 2) * bodyB.getAngMass() / amBoth;
        bodyB.zAngVelocity += (relZVelFromPerpVel - relZVelocity / 2) * bodyA.getAngMass() / amBoth;

        return relVProject;
    }
}
