/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.physics;

/**
 *
 * @author mush
 */
public class CircularBodyEdge implements BodyEdge {

    public double radius;

    public CircularBodyEdge(double radius) {
        this.radius = radius;
    }

    @Override
    public double getDistance(FreeBody body, double dx, double dy) {
        return radius;
    }

    @Override
    public double getWidth() {
        return radius * 2;
    }

    @Override
    public double getHeight() {
        return radius * 2;
    }
}
