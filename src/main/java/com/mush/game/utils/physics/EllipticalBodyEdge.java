/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.physics;

/**
 *
 * @author mush
 */
public class EllipticalBodyEdge implements BodyEdge {

    public double radiusX;
    public double radiusY;

    public EllipticalBodyEdge(double radiusX, double radiusY) {
        this.radiusX = radiusX;
        this.radiusY = radiusY;
    }

    @Override
    public double getDistance(FreeBody body, double dx, double dy) {
        double ldx = body.getGlobalToLocalX(dx, dy);
        double ldy = body.getGlobalToLocalY(dx, dy);

//        System.out.println(dx + " " + dy + " -> " + ldx + " " + ldy + " rd " + radiusX + " " + radiusY + " rt " + body.zRotation);
        ldx *= radiusX;
        ldy *= radiusY;
        return Math.sqrt(ldx * ldx + ldy * ldy);
//        return Math.abs(ldx) * radiusX + Math.abs(ldy) * radiusY;
    }

    @Override
    public double getWidth() {
        return radiusX * 2;
    }

    @Override
    public double getHeight() {
        return radiusY * 2;
    }
}
