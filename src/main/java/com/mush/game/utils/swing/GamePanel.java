/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.swing;

import com.mush.game.utils.render.GameRenderer;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;

/**
 *
 * @author mush
 */
public class GamePanel extends JPanel {

    private GameRenderer renderer;

    GamePanel(GameRenderer renderer) {
        super();
        this.renderer = renderer;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        renderer.draw((Graphics2D) g);
    }

}
