/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.swing;

import com.mush.game.utils.core.Updateable;
import com.mush.game.utils.render.GameRenderer;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mush
 */
public class GameRefreshThread implements Runnable {

    private static final long MICRO = 1000000;
    public static final long NANO = 1000000000;
    private static final int DEFAULT_FPS = 30;

    private Updateable game;
    private GameRenderer renderer = null;
    private BufferStrategy bufferStrategy = null;
    private GamePanel panel = null;

    private boolean isRunning = true;
    private boolean isUpdating = true;
    private boolean isRefresing = true;
    private boolean isInsane = false;
    private long targetNanos;
    private double currentFrameFps;
    private double averageFps;

    public GameRefreshThread(Updateable game, GameRenderer renderer, BufferStrategy bs) {
        this.game = game;
        this.renderer = renderer;
        this.bufferStrategy = bs;
        setTargetFps(DEFAULT_FPS);
    }

    public GameRefreshThread(Updateable game, GameRenderer renderer, GamePanel panel) {
        this.game = game;
        this.renderer = renderer;
        this.panel = panel;
        setTargetFps(DEFAULT_FPS);
    }

    public final void setTargetFps(int targetFps) {
        targetNanos = targetFps <= 0
                ? 0
                : NANO / targetFps;
        currentFrameFps = 0;
        averageFps = 0;
    }

    public void stop() {
        isRunning = false;
    }

    public void setUpdating(boolean b) {
        isUpdating = b;
    }

    public void setRefreshing(boolean b) {
        isRefresing = b;
    }

    public void setTimekeepingInsanity(boolean b) {
        isInsane = b;
    }

    @Override
    public void run() {
        long tick = System.nanoTime();
        long lastTick;
        while (isRunning) {
            lastTick = tick;
            tick = System.nanoTime();
            long elapsedNanos = tick - lastTick;
            long overheadNanos = elapsedNanos - targetNanos;
            long availableNanos = overheadNanos > 0 ? targetNanos - overheadNanos : targetNanos;
            double elapsedSeconds = (double) elapsedNanos / NANO;

            update(elapsedSeconds);

            if (targetNanos > 0) {
                long tock = System.nanoTime();
                long spentNanos = tock - tick;
                long remainNanos = availableNanos - spentNanos;

                long sleepMillis = remainNanos / MICRO;

                if (sleepMillis > 0) {
                    if (isInsane) {
                        long sleptNanos = 0;
                        while (sleptNanos < remainNanos) {
                            try {
                                Thread.sleep(0);
                            } catch (InterruptedException e) {
                            }
                            sleptNanos = System.nanoTime() - tock;
                        }
                    } else {
                        try {
                            // Since sleep is so unreliable, 
                            // sleep for only half the millis.
                            // And then hope for the best
                            Thread.sleep(sleepMillis / 2);
                        } catch (InterruptedException ex) {
                        }
                    }
                }
            }
        }
    }

    private void update(double elapsedSeconds) {
        updateCurrentFps(elapsedSeconds);

        if (isUpdating) {
            try {
                game.updateCurrentFps(currentFrameFps);
                game.update(elapsedSeconds);
            } catch (Exception e) {
                Logger.getGlobal().log(Level.INFO, "Exception during update", e);
            }

            if (isRefresing) {
                if (renderer != null) {
                    renderer.upadteFps(currentFrameFps);
                    renderer.render();
                }
                if (panel != null) {
                    panel.repaint();
                }
            }
        }

        if (bufferStrategy != null) {
            Graphics g = bufferStrategy.getDrawGraphics();
            renderer.draw((Graphics2D) g);
            bufferStrategy.show();
            g.dispose();
        }
    }

    private void updateCurrentFps(double elapsedSeconds) {
        currentFrameFps = elapsedSeconds > 0
                ? 1.0 / elapsedSeconds
                : 1000;
        averageFps = averageFps * 0.9 + currentFrameFps * 0.1;
    }

}
