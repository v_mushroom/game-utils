/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.swing;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 *
 * @author mush
 */
public class GameWindowListener implements WindowListener {

    private final Game game;

    public GameWindowListener(Game game) {
        this.game = game;
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        game.getRefreshThread().stop();
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
        if (!game.shouldRenderWhenMinimized()) {
            game.getRefreshThread().setRefreshing(false);
        }
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        game.getRefreshThread().setRefreshing(true);
    }

    @Override
    public void windowActivated(WindowEvent e) {
        game.getRefreshThread().setUpdating(true);
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        if (game.shouldPauseOnLoseFocus()) {
            game.getRefreshThread().setUpdating(false);
        }
    }

}
