/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.swing;

import com.mush.game.utils.core.GameKeyboard;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author mush
 */
public class GameKeyListener implements KeyListener {

    private Set<GameKeyboard> gameKeyboards = new HashSet<>();

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        for (GameKeyboard gameKeyboard : gameKeyboards) {
            gameKeyboard.keyCodePressed(keyCode);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int keyCode = e.getKeyCode();
        for (GameKeyboard gameKeyboard : gameKeyboards) {
            gameKeyboard.keyCodeReleased(keyCode);
        }
    }

    public void addKeyboard(GameKeyboard gameKeyboard) {
        gameKeyboards.add(gameKeyboard);
    }

    public void removeKeyboard(GameKeyboard gameKeyboard) {
        gameKeyboards.remove(gameKeyboard);
    }

}
