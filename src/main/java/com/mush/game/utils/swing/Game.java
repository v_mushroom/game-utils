/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.swing;

import com.mush.game.utils.core.Updateable;
import com.mush.game.utils.render.GameRenderer;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JPanel;

/**
 *
 * @author mush
 */
public class Game {

    private Updateable updateable;
    private GameRenderer renderer;
    private KeyListener keyListener;
    private JPanel panel;
    private GameRefreshThread refreshThread;
    private JFrame frame;

    private boolean pauseOnLoseFocus = false;
    private boolean renderWhenMinimized = false;
    private boolean useActiveGraphics = false;
    private boolean frameUndecorated = false;

    public String frameTitle = "Title";
    public String frameIcon = "img/icon.png";
    public Dimension preferredSize = new Dimension(100, 100);

    static {
        setupLogging();
    }

    public Game(Updateable updateable, GameRenderer renderer, KeyListener keyListener) {
        this.updateable = updateable;
        this.renderer = renderer;
        this.keyListener = keyListener;
    }

    public static void setupLogging() {
        setupLogging("logging.properties");
    }
    
    public static void setupLogging(String filename) {
        try {
            InputStream stream = Game.class.getClassLoader().
                    getResourceAsStream(filename);
            if (stream != null) {
                LogManager.getLogManager().readConfiguration(stream);
            } else {
                System.setProperty("java.util.logging.SimpleFormatter.format",
                        "[%1$tF %1$tT] [%4$-7s] %5$s %n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createFrame() {
        frame = new JFrame(frameTitle);

        frame.addWindowListener(new GameWindowListener(this));

        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setUndecorated(frameUndecorated);

        ImageIcon img = new ImageIcon(frameIcon);
        frame.setIconImage(img.getImage());

        if (keyListener != null) {
            frame.addKeyListener(keyListener);
        }

        panel = useActiveGraphics
                ? new JPanel()
                : new GamePanel(renderer);
        panel.setVisible(true);

        panel.setPreferredSize(preferredSize);

        frame.add(panel);

        frame.pack();

        frame.setLocationRelativeTo(null);

        if (useActiveGraphics) {
            frame.setIgnoreRepaint(true);
            frame.createBufferStrategy(3);
        }
    }

    public void close() {
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
    }

    public void resize(int width, int height) {
        renderer.setScreenSize(width, height);
        preferredSize.width = width;
        preferredSize.height = height;
        panel.setPreferredSize(preferredSize);
        frame.setPreferredSize(preferredSize);
        frame.pack();
        frame.setLocationRelativeTo(null);
    }

    public Dimension getDisplaySize(Dimension dimension0) {
        Dimension dimension = dimension0 != null ? dimension0 : new Dimension();
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        dimension.width = gd.getDisplayMode().getWidth();
        dimension.height = gd.getDisplayMode().getHeight();
        return dimension;
    }

    public void resizeToScreen() {
        Dimension display = getDisplaySize(null);
        resize(display.width, display.height);
    }

    private void startRefreshThread() {
        if (useActiveGraphics) {
            refreshThread = new GameRefreshThread(updateable, renderer, frame.getBufferStrategy());
        } else {
            refreshThread = new GameRefreshThread(updateable, renderer, (GamePanel) panel);
        }
        Thread t = new Thread(refreshThread);
        t.setName("Game Refresh Thread");
        t.start();
    }

    public void start() {
        createFrame();
        startRefreshThread();

        frame.setVisible(true);
    }

    public GameRefreshThread getRefreshThread() {
        return refreshThread;
    }

    boolean shouldPauseOnLoseFocus() {
        return pauseOnLoseFocus;
    }

    public void setPauseOnLoseFocus(boolean pauseOnLoseFocus) {
        this.pauseOnLoseFocus = pauseOnLoseFocus;
    }

    boolean shouldRenderWhenMinimized() {
        return renderWhenMinimized;
    }

    public void setRenderWhenMinimized(boolean renderWhenMinimized) {
        this.renderWhenMinimized = renderWhenMinimized;
    }

    public void setUseActiveGraphics(boolean useActiveGraphics) {
        this.useActiveGraphics = useActiveGraphics;
    }

    public void setFrameUndecorated(boolean frameUndecorated) {
        this.frameUndecorated = frameUndecorated;
    }

}
