/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.core;

/**
 *
 * @author mush
 */
public class GameInputEvent {

    public final Object message;

    private GameInputEvent(Object message) {
        this.message = message;
    }

    public static class Action extends GameInputEvent {

        public Action(Object message) {
            super(message);
        }
    }

    public static class State extends GameInputEvent {

        public final boolean active;

        public State(Object message, boolean active) {
            super(message);
            this.active = active;
        }
    }
}
