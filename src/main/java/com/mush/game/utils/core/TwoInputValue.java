/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.core;

/**
 *
 * @author mush
 */
public class TwoInputValue {
    
    private double inputA;
    private double inputB;
    private double output;
    
    public TwoInputValue() {
        setInputA(0);
        setInputB(0);
    }

    public void setInputA(double inputA) {
        this.inputA = inputA;
        evaluate();
    }

    public void setInputB(double inputB) {
        this.inputB = inputB;
        evaluate();
    }
    
    private void evaluate() {
        output = inputA - inputB;
    }

    public double getOutput() {
        return output;
    }
    
}
