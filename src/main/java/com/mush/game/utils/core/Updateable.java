/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.core;

/**
 *
 * @author mush
 */
public interface Updateable {

    public void update(double elapsedSeconds);

    public void updateCurrentFps(double fps);
}
