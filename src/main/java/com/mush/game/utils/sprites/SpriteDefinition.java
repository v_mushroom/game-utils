/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.sprites;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mush
 */
public class SpriteDefinition {

    private Map<String, SpriteAnimation> stateAnimationMap;
    private Map<String, int[]> stateCenterMap;

    public SpriteDefinition() {
        stateAnimationMap = new HashMap<>();
        stateCenterMap = new HashMap<>();
    }

    public void setAnimationForState(String state, SpriteAnimation animation) {
        stateAnimationMap.put(state, animation);
    }

    public void setCenterForState(String state, Integer centerX, Integer centerY) {
        stateCenterMap.put(state, new int[]{
            centerX != null ? centerX : 0,
            centerY != null ? centerY : 0
        });
    }

    public SpriteAnimation getAnimationForState(String state) {
        return stateAnimationMap.get(state);
    }

    public int[] getCenterForState(String state) {
        int[] center = stateCenterMap.get(state);
        if (center == null) {
            center = new int[]{0, 0};
        }
        return center;
    }

}
