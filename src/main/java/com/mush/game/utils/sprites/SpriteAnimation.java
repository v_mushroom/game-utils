/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.sprites;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mush
 */
public class SpriteAnimation {

    public static class Advancement {

        public int frameIndex;
        public double secondsSinceFrame;
        private boolean finished = false;
        private int repeated;

        public Advancement() {
            reset();
        }

        public boolean isFinished() {
            return finished;
        }
        
        public final void reset() {
            finished = false;
            frameIndex = 0;
            secondsSinceFrame = 0;
            repeated = 0;
        }
    }

    private List<BufferedImage> frames;
    private List<Double> frameDurations;
    private int frameCount;
    private double totalDuration;
    private boolean repeats = true;
    private int repeatCount;

    public SpriteAnimation() {
        frames = new ArrayList<>();
        frameDurations = new ArrayList<>();
        frameCount = 0;
        repeatCount = 0;
        totalDuration = 0;
    }

    public SpriteAnimation(BufferedImage frame) {
        this();
        addFrame(frame, 0);
    }

    public void addFrame(BufferedImage frame, double duration) {
        frames.add(frame);
        frameDurations.add(duration);
        frameCount++;
        totalDuration += duration;
    }

    public BufferedImage getFrame(int animationFrameIndex) {
        return frames.get(animationFrameIndex);
    }

    public boolean hasMultipleFrames() {
        return frames.size() > 1;
    }

    public double getTotalDuration() {
        return totalDuration;
    }

    public void setRepeats(boolean repeats) {
        this.repeats = repeats;
    }
    
    public boolean repeats() {
        return this.repeats;
    }
    
    public void setRepeatCount(int repeatCount) {
        this.repeatCount = repeatCount;
    }

    public void advance(int animationFrameIndex, double secondsSinceFrame, Advancement result) {
        if (result.finished) {
            return;
        }

        int frameIndex = animationFrameIndex;
        double frameDuration = frameDurations.get(frameIndex);
        double remainingTime = secondsSinceFrame;

        if (frameDuration > 0) {
            while (remainingTime > frameDuration) {
                remainingTime -= frameDuration;
                frameIndex++;
                if (frameIndex >= frameCount) {
                    if (repeats) {
                        if (repeatCount > 0 && result.repeated >= repeatCount) {
                            result.finished = true;
                        } else {
                            frameIndex = 0;
                            result.repeated++;
                        }
                    } else {
                        result.finished = true;
                    }
                }
            }
        }
        
        if (result.finished) {
            frameIndex = frameCount - 1;
        }

        result.frameIndex = frameIndex;
        result.secondsSinceFrame = remainingTime;
    }

}
