/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.sprites;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mush
 */
public class SpriteFactory {

    public enum SpriteCenter {
        CENTER
    }

    public static class SpriteSheetConfiguration {

        public String spriteSheet;
        public Integer spriteWidth;
        public Integer spriteHeight;
        public Integer spriteCenterX;
        public Integer spriteCenterY;
        public Integer spriteOffsetX;
        public Integer spriteOffsetY;
        public SpriteCenter spriteCenter;

    }

    public static class Configuration extends SpriteSheetConfiguration {

        public static class Type extends SpriteSheetConfiguration {

            public Map<String, State> spriteStates;
        }

        public static class State extends SpriteSheetConfiguration {

            public List<double[]> frames;
            public boolean repeats = true;
            public int repeatCount = 0;
        }

        public Map<String, Type> spriteTypes;

    }

    private Map<String, SpriteDefinition> typeDefinitionMap;
    private Map<String, SpriteSheet> spriteSheetMap;
    private final Logger logger = Logger.getGlobal();

    public SpriteFactory() {
        typeDefinitionMap = new HashMap<>();
        spriteSheetMap = new HashMap<>();
    }

    public void load(String jsonFileName) throws IOException {
        logger.log(Level.INFO, "Loading sprites from [{0}]", jsonFileName);
        Configuration config = loadJson(jsonFileName);
//        spriteSheetMap = new HashMap<>();

        for (Map.Entry<String, Configuration.Type> spriteType : config.spriteTypes.entrySet()) {
            parseType(spriteType.getKey(), spriteType.getValue(), config);
        }

        spriteSheetMap.clear();
    }

    private void parseType(String type, Configuration.Type typeConfig, Configuration config) throws IOException {
        SpriteDefinition typeDefinition = new SpriteDefinition();
        for (Map.Entry<String, Configuration.State> spriteState : typeConfig.spriteStates.entrySet()) {
            parseState(spriteState.getKey(), spriteState.getValue(), typeConfig, config, typeDefinition);
        }
        addSpriteType(type, typeDefinition);
    }

    public SpriteSheet getSpriteSheet(String spriteSheetFile, Integer spriteWidth, Integer spriteHeight) throws IOException {
        return getSpriteSheet(spriteSheetFile, spriteWidth, spriteHeight, 0, 0);
    }

    public SpriteSheet getSpriteSheet(String spriteSheetFile, Integer spriteWidth, Integer spriteHeight, Integer spriteOffsetX, Integer spriteOffsetY) throws IOException {
        String key = spriteSheetFile + "_" + spriteWidth + "x" + spriteHeight + "_" + spriteOffsetX + "_" + spriteOffsetY;
        SpriteSheet spriteSheet = spriteSheetMap.get(key);

        if (spriteSheet == null) {
            spriteSheet = new SpriteSheet(spriteSheetFile, spriteWidth, spriteHeight, spriteOffsetX, spriteOffsetY);
            spriteSheetMap.put(key, spriteSheet);
        }

        return spriteSheet;
    }

    private void parseState(String state, Configuration.State stateConfig, Configuration.Type typeConfig, Configuration config, SpriteDefinition typeDefinition) throws IOException {
        String spriteSheetFile = getSpriteSheet(stateConfig, typeConfig, config);
        Integer spriteWidth = getSpriteWidth(stateConfig, typeConfig, config);
        Integer spriteHeight = getSpriteHeight(stateConfig, typeConfig, config);
        Integer spriteCenterX = getSpriteCenterX(stateConfig, typeConfig, config);
        Integer spriteCenterY = getSpriteCenterY(stateConfig, typeConfig, config);
        Integer spriteOffsetX = getSpriteOffsetX(stateConfig, typeConfig, config);
        Integer spriteOffsetY = getSpriteOffsetY(stateConfig, typeConfig, config);
        SpriteCenter spriteCenter = getSpriteCenter(stateConfig, typeConfig, config);

        if (spriteSheetFile == null || spriteWidth == null || spriteHeight == null) {
            return;
        }

        SpriteSheet spriteSheet = getSpriteSheet(spriteSheetFile, spriteWidth, spriteHeight, spriteOffsetX, spriteOffsetY);

        SpriteAnimation animation = new SpriteAnimation();

        for (double[] frame : stateConfig.frames) {
            int u = (int) frame[0];
            int v = (int) frame[1];
            double duration = frame[2];

            BufferedImage image = spriteSheet.cutImage(u, v);
            if (image != null) {
                animation.addFrame(image, duration);
            }
        }

        animation.setRepeats(stateConfig.repeats);
        animation.setRepeatCount(stateConfig.repeatCount);

        typeDefinition.setAnimationForState(state, animation);
        logger.log(Level.FINE, "parse state: {0} {1} {2} {3}", new Object[]{state, spriteCenterX, spriteCenterY, spriteCenter});

        if (spriteCenter == SpriteCenter.CENTER) {
            if (spriteCenterX == null) {
                spriteCenterX = spriteWidth / 2;
            }
            if (spriteCenterY == null) {
                spriteCenterY = spriteHeight / 2;
            }
        }
        typeDefinition.setCenterForState(state, spriteCenterX, spriteCenterY);
    }

    private String getSpriteSheet(Configuration.State stateConfig, Configuration.Type typeConfig, Configuration config) {
        String spriteSheet = stateConfig.spriteSheet != null
                ? stateConfig.spriteSheet
                : typeConfig.spriteSheet;
        spriteSheet = spriteSheet != null
                ? spriteSheet
                : config.spriteSheet;
        return spriteSheet;
    }

    private Integer getSpriteWidth(Configuration.State stateConfig, Configuration.Type typeConfig, Configuration config) {
        Integer spriteWidth = stateConfig.spriteWidth != null
                ? stateConfig.spriteWidth
                : typeConfig.spriteWidth;
        spriteWidth = spriteWidth != null
                ? spriteWidth
                : config.spriteWidth;
        return spriteWidth;
    }

    private Integer getSpriteHeight(Configuration.State stateConfig, Configuration.Type typeConfig, Configuration config) {
        Integer spriteHeight = stateConfig.spriteHeight != null
                ? stateConfig.spriteHeight
                : typeConfig.spriteHeight;
        spriteHeight = spriteHeight != null
                ? spriteHeight
                : config.spriteHeight;
        return spriteHeight;
    }

    private Integer getSpriteCenterX(Configuration.State stateConfig, Configuration.Type typeConfig, Configuration config) {
        Integer spriteCenterX = stateConfig.spriteCenterX != null
                ? stateConfig.spriteCenterX
                : typeConfig.spriteCenterX;
        spriteCenterX = spriteCenterX != null
                ? spriteCenterX
                : config.spriteCenterX;
        return spriteCenterX;
    }

    private Integer getSpriteCenterY(Configuration.State stateConfig, Configuration.Type typeConfig, Configuration config) {
        Integer spriteCenterY = stateConfig.spriteCenterY != null
                ? stateConfig.spriteCenterY
                : typeConfig.spriteCenterY;
        spriteCenterY = spriteCenterY != null
                ? spriteCenterY
                : config.spriteCenterY;
        return spriteCenterY;
    }

    private SpriteCenter getSpriteCenter(Configuration.State stateConfig, Configuration.Type typeConfig, Configuration config) {
        SpriteCenter spriteCenter = stateConfig.spriteCenter != null
                ? stateConfig.spriteCenter
                : typeConfig.spriteCenter;
        spriteCenter = spriteCenter != null
                ? spriteCenter
                : config.spriteCenter;
        return spriteCenter;
    }
    
    private Integer getSpriteOffsetX(Configuration.State stateConfig, Configuration.Type typeConfig, Configuration config) {
        Integer spriteOffsetX = stateConfig.spriteOffsetX != null
                ? stateConfig.spriteOffsetX
                : typeConfig.spriteOffsetX;
        spriteOffsetX = spriteOffsetX != null
                ? spriteOffsetX
                : config.spriteOffsetX;
        spriteOffsetX = spriteOffsetX != null
                ? spriteOffsetX
                : 0;
        return spriteOffsetX;
    }

    private Integer getSpriteOffsetY(Configuration.State stateConfig, Configuration.Type typeConfig, Configuration config) {
        Integer spriteOffsetY = stateConfig.spriteOffsetY != null
                ? stateConfig.spriteOffsetY
                : typeConfig.spriteOffsetY;
        spriteOffsetY = spriteOffsetY != null
                ? spriteOffsetY
                : config.spriteOffsetY;
        spriteOffsetY = spriteOffsetY != null
                ? spriteOffsetY
                : 0;
        return spriteOffsetY;
    }

    public void addSpriteType(String type, SpriteDefinition definition) {
        typeDefinitionMap.put(type, definition);
    }

    public Sprite createSprite(String type) {
        SpriteDefinition definition = typeDefinitionMap.get(type);
        if (definition != null) {
            return new Sprite(definition);
        } else {
            return null;
        }
    }

    private Configuration loadJson(String fileName) throws IOException {
        File jsonFile = new File(fileName);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Configuration configuration = mapper.readValue(jsonFile, Configuration.class);
        return configuration;
    }

}
