/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.sprites;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author mush
 */
public class SpriteSheet {

    private BufferedImage image;
    private int defaultSpriteWidth;
    private int defaultSpriteHeight;
    private int defaultSpriteOffsetX;
    private int defaultSpriteOffsetY;
    // TODO: optimize multiple cuts of same sprite

    public SpriteSheet(String fileName, int spriteWidth, int spriteHeight) throws IOException {
        this(fileName, spriteWidth, spriteHeight, 0, 0);
    }

    public SpriteSheet(String fileName, int spriteWidth, int spriteHeight, int spriteOffsetX, int spriteOffsetY) throws IOException {
        image = ImageIO.read(new File(fileName));
        defaultSpriteWidth = spriteWidth;
        defaultSpriteHeight = spriteHeight;
        defaultSpriteOffsetX = spriteOffsetX;
        defaultSpriteOffsetY = spriteOffsetY;
    }

    public BufferedImage cutImage(int u, int v) {
        return cutImage(u, v, defaultSpriteWidth, defaultSpriteHeight);
    }

    public BufferedImage cutImage(int u, int v, int spriteWidth, int spriteHeight) {
        BufferedImage tile = new BufferedImage(spriteWidth, spriteHeight, image.getType());
        Graphics2D g = tile.createGraphics();
        g.drawImage(image, -u * spriteWidth - defaultSpriteOffsetX, -v * spriteHeight - defaultSpriteOffsetY, null);
        g.dispose();
        return tile;
    }

    public int getDefaultSpriteHeight() {
        return defaultSpriteHeight;
    }

    public int getDefaultSpriteWidth() {
        return defaultSpriteWidth;
    }

}
