/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.sprites;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Objects;

/**
 *
 * @author mush
 */
public class Sprite {

    private final SpriteDefinition definition;
    private String state;
    private SpriteAnimation animation;
    private final SpriteAnimation.Advancement animationAdvancement;
    private int animationFrameIndex;
    private double secondsSinceFrame;
    private BufferedImage currentFrame;
    private int frameCenterX = 0;
    private int frameCenterY = 0;

    public Sprite(SpriteDefinition definition) {
        animationAdvancement = new SpriteAnimation.Advancement();
        this.definition = definition;
        this.state = null;
        setAnimation(null);
    }

    public void update(double elapsedSeconds) {
        if (animation == null) {
            return;
        }

        if (animation.hasMultipleFrames() || !animation.repeats()) {
            secondsSinceFrame += elapsedSeconds;

            animation.advance(animationFrameIndex, secondsSinceFrame, animationAdvancement);

            if (animationFrameIndex != animationAdvancement.frameIndex) {
                setFrame(animationAdvancement.frameIndex, animationAdvancement.secondsSinceFrame);
            }
        }
    }

    public BufferedImage getImage() {
        return currentFrame;
    }
    
    public boolean isAnimationFinished() {
        return animationAdvancement.isFinished();
    }

    public void draw(Graphics2D g, int x, int y) {
        if (currentFrame != null) {
            g.drawImage(currentFrame, x - frameCenterX, y - frameCenterY, null);
        }
    }

    public void setAnimation(SpriteAnimation animation) {
        this.animation = animation;
        if (this.animation != null) {
            setFrame(0, 0);
            animationAdvancement.reset();
        } else {
            currentFrame = null;
        }
    }
    
    private void setFrameCenter(int[] center) {
        setFrameCenter(center[0], center[1]);
    }
    
    public Double getAnimationDuration() {
        if (this.animation != null) {
            return this.animation.getTotalDuration();
        } else {
            return null;
        }
    }
    
    public void setFrameCenter(int x, int y) {
        frameCenterX = x;
        frameCenterY = y;
    }

    private void setFrame(int frameIndex, double elapsedSeconds) {
        animationFrameIndex = frameIndex;
        secondsSinceFrame = elapsedSeconds;
        currentFrame = animation.getFrame(animationFrameIndex);
    }

    public void setState(String newState) {
        if (definition == null) {
            return;
        }
        if (!Objects.equals(newState, this.state)) {
            this.state = newState;
            setAnimation(definition.getAnimationForState(this.state));
            setFrameCenter(definition.getCenterForState(this.state));
        }
    }
    
    public String getState() {
        return this.state;
    }
}
