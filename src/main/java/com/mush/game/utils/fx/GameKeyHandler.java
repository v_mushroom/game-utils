/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.fx;

import com.mush.game.utils.core.GameKeyboard;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author mush
 */
public class GameKeyHandler extends GameKeyboard implements EventHandler<KeyEvent> {

    public void bindActionKey(KeyCode keyCode, Object message) {
        super.bindActionKey(keyCode.ordinal(), message);
    }

    public void bindStateKey(KeyCode keyCode, Object message) {
        super.bindStateKey(keyCode.ordinal(), message);
    }

    @Override
    public void handle(KeyEvent e) {
        if (KeyEvent.KEY_PRESSED.equals(e.getEventType())) {
            keyCodePressed(e.getCode().ordinal());
        }
        if (KeyEvent.KEY_RELEASED.equals(e.getEventType())) {
            keyCodeReleased(e.getCode().ordinal());
        }
    }

}
