/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.fx;

import com.mush.game.utils.core.Updateable;
import com.mush.game.utils.render.GameRenderer;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;

/**
 *
 * @author mush
 */
public abstract class GameApplication extends Application {

    public abstract Updateable geUpdateable();

    public abstract GameRenderer getGameRenderer();

    public abstract GameKeyHandler getGameKeyHandler();

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Timeline Example");

        Group root = new Group();
        Scene theScene = new Scene(root);
        primaryStage.setScene(theScene);

        Canvas canvas = new Canvas(512, 512);
        root.getChildren().add(canvas);

        GraphicsContext gc = canvas.getGraphicsContext2D();

        final GameApplication app = this;

        new AnimationTimer() {

            long lastNanoTime = System.nanoTime();
            WritableImage image = null;

            public void handle(long currentNanoTime) {
                double elapsedSeconds = (currentNanoTime - lastNanoTime) / 1000000000.0;
                double currentFrameFps = elapsedSeconds > 0
                        ? 1.0 / elapsedSeconds
                        : 1000;

                lastNanoTime = currentNanoTime;

                Updateable updateable = app.geUpdateable();
                updateable.update(elapsedSeconds);
                updateable.updateCurrentFps(currentFrameFps);

                GameRenderer renderer = app.getGameRenderer();
                renderer.upadteFps(currentFrameFps);
                renderer.render();

//                image = renderer.draw(gc, image);
                image = FxGameRenderer.draw(renderer, gc, image);
            }
        }.start();


        theScene.setOnKeyPressed(getGameKeyHandler());
        theScene.setOnKeyReleased(getGameKeyHandler());

        primaryStage.show();
    }

}
