/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.fx;

import com.mush.game.utils.render.GameRenderer;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.WritableImage;

/**
 *
 * @author mush
 */
public class FxGameRenderer {

    public static final WritableImage draw(GameRenderer renderer, GraphicsContext gc, WritableImage image0) {
        WritableImage image = SwingFXUtils.toFXImage(renderer.getScreenImage(), image0);
        gc.drawImage(image, 0, 0);

        if (renderer.getShowFps()) {
            gc.setLineWidth(1);
            gc.setFill(javafx.scene.paint.Color.LIMEGREEN);
            gc.fillText("" + Math.round(renderer.getFps()), 1, 13);
        }

        return image;
    }

}
