/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map.material;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Declaration of map materials and their properties
 *
 * @author mush
 */
public class MapMaterials {

    private final Map<Integer, MapMaterial> map;
    private final Map<String, Integer> idsByName;
    private final AtomicInteger lastId = new AtomicInteger(0);

    public MapMaterials() {
        map = new HashMap<>();
        idsByName = new HashMap<>();
    }

    public Integer addMaterial(String name, Map<String, Object> attributes) {
        int id = lastId.addAndGet(1);
        MapMaterial material = new MapMaterial(id, name, attributes);
        map.put(id, material);
        idsByName.put(name, id);
        return id;
    }

    public MapMaterial getMaterialById(Integer id) {
        return map.get(id);
    }

    public Set<String> getMaterialNames() {
        return idsByName.keySet();
    }

    public MapMaterial getMaterialByName(String materialName) {
        Integer id = idsByName.get(materialName);
        if (id == null) {
            return null;
        }
        return map.get(id);
    }

}
