/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map.move;

import com.mush.game.utils.map.move.MapContentCollision.MapItemCollision;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;

/**
 *
 * @author mush
 * @param <M> map item type, for map collision checking
 */
public class MovingObjects<M> {

    private final Map<Integer, MovingObject> mobs;
    private final AtomicInteger nextMobId = new AtomicInteger(0);

    public MovingObjects() {
        mobs = new HashMap<>();
    }

    public int add(MovingObject mob) {
        mob.id = nextMobId.addAndGet(1);
        mobs.put(mob.id, mob);
        return mob.id;
    }

    public MovingObject remove(int id) {
        MovingObject mob = mobs.remove(id);
        if (mob != null) {
            mob.id = null;
        }
        return mob;
    }

    public MovingObject get(int id) {
        return mobs.get(id);
    }

    public Collection<MovingObject> getAll() {
        return Collections.unmodifiableCollection(mobs.values());
    }

    public void beforeCollisionChecking(double elapsedSeconds) {
        mobs.forEach((id, mob) -> {
            mob.prepareMovement(elapsedSeconds);
            mob.updateMovingBox();
        });
    }

    public void checkMapCollisions(
            MapContentCollision<M> contentCollision,
            BiConsumer<MovingObject, Set<MapItemCollision<M>>> action) {

        mobs.forEach((id, mob) -> {
            if (mob.hasMovement()) {
                Set<MapItemCollision<M>> collisions = contentCollision.checkMapItemMovementCollisions(mob.movingBox, mob.movement);
                if (!collisions.isEmpty()) {
                    action.accept(mob, collisions);
                }
            }
        });
    }

    public void checkMobCollisions(BiConsumer<MovingObject, Set<Integer>> action) {
        mobs.forEach((id, mob) -> {
            if (mob.hasMovement()) {
                Set<Integer> collisions = new HashSet<>();
                mobs.forEach((id2, mob2) -> {
                    if (!Objects.equals(id, id2)) {
                        checkMoveCollision(mob, mob2, collisions);
                    }
                });
                if (!collisions.isEmpty()) {
                    action.accept(mob, collisions);
                }
            }
        });
    }

    public void afterCollisionChecking() {
        mobs.forEach((id, mob) -> {
            mob.applyMovementToPosition();
        });
    }

    private void checkMoveCollision(MovingObject mobA, MovingObject mobB, Set<Integer> collisions) {
        if (!mobA.collidesWithMobs || !mobB.collidesWithMobs) {
            return;
        }

        double prevMovementX = mobA.movement.x;
        double prevMovementY = mobA.movement.y;

        if (BoxCollision.checkMoveCollision(mobA.movingBox, mobB.movingBox, mobA.movement, mobB.isObstacleForMobs)) {
            double movementCorrectionX = mobA.movement.x - prevMovementX;
            double movementCorrectionY = mobA.movement.y - prevMovementY;

            mobA.movingBox.offsetBy(movementCorrectionX, movementCorrectionY);

            collisions.add(mobB.id);
        }
    }

}
