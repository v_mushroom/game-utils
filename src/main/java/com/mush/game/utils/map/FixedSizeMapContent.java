/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map;

/**
 *
 * @author mush
 */
public class FixedSizeMapContent<T> implements MapContent<T> {

    private int width;
    private int height;
    private Object[] items;

    public FixedSizeMapContent() {
        this(0, 0);
    }

    public FixedSizeMapContent(int width, int height) {
        this.width = width;
        this.height = height;
        items = new Object[width * height];
    }

    /**
     * Resizes map, clears previous content
     *
     * @param width
     * @param height
     */
    public void resize(int width, int height) {
        clear(null);
        items = new Object[width * height];
        this.width = width;
        this.height = height;
    }

    @Override
    public T getItemAt(int u, int v) {
        int i = index(u, v);
        return i >= 0 ? (T) items[i] : null;
    }

    @Override
    public void setItemAt(int u, int v, T item) {
        int i = index(u, v);
        if (i >= 0) {
            items[i] = item;
        }
    }

    private int index(int u, int v) {
        if (u >= 0 && u < getWidth() && v >= 0 && v < getHeight()) {
            return v * getWidth() + u;
        } else {
            return -1;
        }
    }

    public void clear(T item) {
        for (int i = 0; i < items.length; i++) {
            items[i] = item;
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
