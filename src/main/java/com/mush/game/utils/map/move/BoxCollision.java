/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map.move;

import java.awt.geom.Point2D;

/**
 *
 * @author mush
 */
public class BoxCollision {

    public static boolean checkMoveCollision(Box boxA, Box boxB, Point2D.Double movement) {
        return checkMoveCollision(boxA, boxB, movement, true);
    }
    
    public static boolean checkMoveCollision(Box boxA, Box boxB, Point2D.Double movement, boolean isObstacle) {
        if (boxA == null || boxB == null) {
            return false;
        }

        double bRightLead = boxB.right - boxA.left;
        double aRightLead = boxA.right - boxB.left;
        double bBottomLead = boxB.bottom - boxA.top;
        double aBottomLead = boxA.bottom - boxB.top;

        if (bRightLead > 0 && aRightLead > 0 && bBottomLead > 0 && aBottomLead > 0) {
            if (movement != null && isObstacle) {
                double correctionX = aRightLead < bRightLead ? -aRightLead : bRightLead;
                double correctionY = aBottomLead < bBottomLead ? -aBottomLead : bBottomLead;
                if (Math.abs(correctionX) > Math.abs(correctionY)) {
                    movement.y += correctionY;
                } else {
                    movement.x += correctionX;
                }
            }
            return true;
        }
        return false;
    }

}
