/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map;

/**
 * Compares if neighboring tile is same as tile
 */
public interface MapTileComparator {

    public boolean isSame(int u, int v, int uOfs, int vOfs);

}
