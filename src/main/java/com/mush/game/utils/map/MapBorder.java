/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map;

/**
 * this is a bit clunky, when possible switch to using MapBoundary
 * 
 * @author mush
 */
public class MapBorder {

    public enum Type {
        TOP,
        TOP_RIGHT,
        RIGHT,
        BOTTOM_RIGHT,
        BOTTOM,
        BOTTOM_LEFT,
        LEFT,
        TOP_LEFT,
        INV_TOP_RIGHT,
        INV_BOTTOM_RIGHT,
        INV_BOTTOM_LEFT,
        INV_TOP_LEFT,
        INV_TOP_RIGHT_BOTTOM_LEFT,
        INV_BOTTOM_RIGHT_TOP_LEFT,
        SINGLE
    }

    //  9 1 3
    //  8 . 2
    // 12 4 6
    public static final Type[] convexTypeOrder = new Type[]{
        null, // 0
        Type.TOP, // 1
        Type.RIGHT, // 2
        Type.TOP_RIGHT, // 3
        Type.BOTTOM, // 4
        null, // 5
        Type.BOTTOM_RIGHT, // 6
        null, // 7
        Type.LEFT, // 8
        Type.TOP_LEFT, // 9
        null, // 10
        null, // 11
        Type.BOTTOM_LEFT, // 12
        null, // 13
        null, // 14
        null, // 15
    };

    // 8   1
    //   .
    // 4   2
    // 5
    // 10
    public static final Type[] concaveTypeOrder = new Type[]{
        null, // 0
        Type.INV_TOP_RIGHT, // 1
        Type.INV_BOTTOM_RIGHT, // 2
        null, // 3
        Type.INV_BOTTOM_LEFT, // 4
        Type.INV_TOP_RIGHT_BOTTOM_LEFT, // 5
        null, // 6
        null, // 7
        Type.INV_TOP_LEFT, // 8
        null, // 9
        Type.INV_BOTTOM_RIGHT_TOP_LEFT, // 10
        null, // 11
        null, // 12
        null, // 13
        null, // 14
        null, // 15
    };

    final static int[] adjacentOfsU = new int[]{0, 1, 0, -1};
    final static int[] adjacentOfsV = new int[]{-1, 0, 1, 0};

    final static int[] diagonalOfsU = new int[]{1, 1, -1, -1};
    final static int[] diagonalOfsV = new int[]{-1, 1, 1, -1};

//    /**
//     *
//     * @deprecated use MapTileComparator
//     */
//    @Deprecated
//    public interface DataProvider {
//
//        public boolean isSame(int u, int v, int uOfs, int vOfs);
//    }

    public static Type getType(int u, int v, MapTileComparator comparator) {
        int count = 0;
        int code = 0;
        int id = 1;

        for (int i = 0; i < adjacentOfsU.length; i++) {
            if (!comparator.isSame(u, v, adjacentOfsU[i], adjacentOfsV[i])) {
                count++;
                code += id;
            }
            id *= 2;
        }

        if (count > 2) {
            // More than 2 adjacent neighbours are not the same
            // We don't support this, so we return a border enclosing this single tile
            return Type.SINGLE;
        }

        if (count == 0) {
            // All adjacent neighbours are the same
            // We need to check the diagonal ones
            return getDiagonalType(u, v, comparator);
        }

        return convexTypeOrder[code];
    }

    private static Type getDiagonalType(int u, int v, MapTileComparator comparator) {
        int count = 0;
        int code = 0;
        int id = 1;

        for (int i = 0; i < diagonalOfsU.length; i++) {
            if (!comparator.isSame(u, v, diagonalOfsU[i], diagonalOfsV[i])) {
                count++;
                code += id;
            }
            id *= 2;
        }

        if (count > 2) {
            // More than 2 diagonal neighbours are not the same
            // We don't support this, so we return a border enclosing this single tile
            return Type.SINGLE;
        }

        if (count == 0) {
            // All diagonal neighbours are the same
            // This is not a border
            return null;
        }

        Type type = concaveTypeOrder[code];

        return type != null
                ? type
                : Type.SINGLE;
    }
}
