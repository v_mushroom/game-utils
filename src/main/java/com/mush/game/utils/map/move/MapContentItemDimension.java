/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map.move;

import java.awt.Point;
import java.awt.geom.Point2D;

/**
 * Relation between discrete map coordinates and real coordinates
 * 
 * @author mush
 */
public class MapContentItemDimension {

    private final double mapItemWidth;
    private final double mapItemHeight;
    private final Box mapItemBounds;

    public MapContentItemDimension(double mapItemWidth, double mapItemHeight) {
        this.mapItemWidth = mapItemWidth;
        this.mapItemHeight = mapItemHeight;
        mapItemBounds = new Box(
                mapItemWidth / 2,
                mapItemHeight / 2,
                mapItemWidth / 2,
                mapItemHeight / 2);
    }

    public Point getUvAtRealCoordinates(double x, double y) {
        int u = (int) (x / mapItemWidth);
        int v = (int) (y / mapItemHeight);
        return new Point(u, v);
    }

    public Point getUvAtRealCoordinates(Point2D.Double position) {
        return getUvAtRealCoordinates(position.x, position.y);
    }

    public Point2D.Double getRealCoordinatesAtUVCenter(int u, int v) {
        return new Point2D.Double(
                getRealXCoordinateAtUVCenter(u), 
                getRealYCoordinateAtUVCenter(v));
    }

    public Box getBoxAtUv(int u, int v, Box reuse) {
        Box box = reuse != null ? reuse : new Box();
        box.recalculateFromBoundsAndPosition(
                mapItemBounds, 
                getRealXCoordinateAtUVCenter(u), 
                getRealYCoordinateAtUVCenter(v));
        return box;
    }

    private double getRealXCoordinateAtUVCenter(int u) {
        return u * mapItemWidth + mapItemWidth / 2;
    }

    private double getRealYCoordinateAtUVCenter(int v) {
        return v * mapItemHeight + mapItemHeight / 2;
    }
}
