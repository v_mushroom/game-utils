/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map.move;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author mush
 */
public class CollidableBoxes {

    private final List<Box> boxes;

    public CollidableBoxes() {
        boxes = new ArrayList<>();
    }

    public void add(Box box) {
        boxes.add(box);
    }

    public void remove(Box box) {
        boxes.remove(box);
    }

    public void update(Box box) {
    }

    public void clear() {
        boxes.clear();
    }

    public List<Box> getAll() {
        return Collections.unmodifiableList(boxes);
    }

    public final List<Box> checkMoveCollision(Box boxA, Point2D.Double movement) {
        List<Box> collisions = new LinkedList<>();

        checkMoveCollision(boxA, movement, collisions);

        return collisions;
    }

    public void checkMoveCollision(Box boxA, Point2D.Double movement, List<Box> collisions) {
        boxes.forEach((boxB) -> {
            if (boxA != boxB) {
                checkMoveCollision(boxA, boxB, movement, collisions);
            }
        });
    }

    private void checkMoveCollision(Box boxA, Box boxB, Point2D.Double movement, List<Box> collisions) {
        double prevMovementX = movement.x;
        double prevMovementY = movement.y;

        if (BoxCollision.checkMoveCollision(boxA, boxB, movement)) {
            double movementCorrectionX = movement.x - prevMovementX;
            double movementCorrectionY = movement.y - prevMovementY;

            boxA.offsetBy(movementCorrectionX, movementCorrectionY);

            collisions.add(boxB);
        }
    }
}
