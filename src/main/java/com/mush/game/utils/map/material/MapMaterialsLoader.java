/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map.material;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mush
 */
public class MapMaterialsLoader {

    public static class Schema {

//{
//    "materials": {
//        "NAME": {
//            "ATTRIBUTE": ?
//        }
//    }
//}
        public Map<String, Map<String, Object>> materials;
    }

    public static MapMaterials loadFromFile(String filePath) throws IOException {
        Schema config = loadJson(filePath);

        MapMaterials materials = new MapMaterials();

        config.materials.forEach((materialName, materialAttributes) -> {
            materials.addMaterial(materialName, materialAttributes);
        });

        return materials;
    }

    private static Schema loadJson(String fileName) throws IOException {
        Logger.getGlobal().log(Level.INFO, "Loading materials from [{0}]", fileName);

        File jsonFile = new File(fileName);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Schema value = mapper.readValue(jsonFile, Schema.class);
        return value;
    }

}
