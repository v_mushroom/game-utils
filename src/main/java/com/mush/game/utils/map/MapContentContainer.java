/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map;

import java.awt.Point;
import java.util.Map;

/**
 *
 * @author mush
 */
public class MapContentContainer {

    private int chunkSize = 8;
    private Object[] chunk;
    private Map<Point, Object[]> chunkMap;

    private int chunkCoordToIndex(int u, int v) {
        return v * chunkSize + u;
    }

    private Point coordToChunkKey(int u, int v) {
        int chunkX = u / chunkSize;
        int chunkY = v / chunkSize;
        return new Point(chunkX, chunkY);
    }
}
