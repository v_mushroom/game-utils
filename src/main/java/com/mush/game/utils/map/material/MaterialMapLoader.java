/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map.material;

import com.mush.game.utils.map.FixedSizeMapContent;
import com.mush.game.utils.map.material.loader.ImageMapLoader;
import com.mush.game.utils.map.material.loader.TextMapLoader;
import java.io.IOException;

/**
 *
 * @author mush
 */
public class MaterialMapLoader {

    public FixedSizeMapContent<MapMaterial> loadFromTextFile(String filePath, MapMaterials materials) throws IOException {
        return TextMapLoader.loadFromFile(filePath, materials);
    }

    public FixedSizeMapContent<MapMaterial> loadFromImageFile(String jsonFilePath, MapMaterials materials) throws IOException {
        return ImageMapLoader.loadFromFile(jsonFilePath, materials);
    }
}
