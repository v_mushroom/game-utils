/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map;

import static com.mush.game.utils.map.MapNeighbours.*;

/**
 *
 * @author mush
 */
public class MapBoundary {

    public enum TileType {
        ALL,
        INV_TOP_LEFT,
        INV_TOP_RIGHT,
        INV_BOTTOM_LEFT,
        INV_BOTTOM_RIGHT,
        INV_TOP_RIGHT_BOTTOM_LEFT,
        INV_BOTTOM_RIGHT_TOP_LEFT,
        TOP,
        BOTTOM,
        LEFT,
        RIGHT,
        TOP_LEFT,
        TOP_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_RIGHT,
        SINGLE
    }

    private static final int[] TYPE_NEIGHBOUR_VALUES;

    static {
        TYPE_NEIGHBOUR_VALUES = new int[TileType.values().length];

        TYPE_NEIGHBOUR_VALUES[TileType.SINGLE.ordinal()] = 0;
        TYPE_NEIGHBOUR_VALUES[TileType.ALL.ordinal()] = MapNeighbours.ALL;

        TYPE_NEIGHBOUR_VALUES[TileType.TOP.ordinal()] = ALL_BOTTOM + LEFT + RIGHT;
        TYPE_NEIGHBOUR_VALUES[TileType.BOTTOM.ordinal()] = ALL_TOP + LEFT + RIGHT;
        TYPE_NEIGHBOUR_VALUES[TileType.LEFT.ordinal()] = TOP + BOTTOM + ALL_RIGHT;
        TYPE_NEIGHBOUR_VALUES[TileType.RIGHT.ordinal()] = TOP + BOTTOM + ALL_LEFT;

        TYPE_NEIGHBOUR_VALUES[TileType.TOP_LEFT.ordinal()] = BOTTOM + RIGHT + BOTTOM_RIGHT;
        TYPE_NEIGHBOUR_VALUES[TileType.TOP_RIGHT.ordinal()] = BOTTOM + LEFT + BOTTOM_LEFT;
        TYPE_NEIGHBOUR_VALUES[TileType.BOTTOM_LEFT.ordinal()] = TOP + RIGHT + TOP_RIGHT;
        TYPE_NEIGHBOUR_VALUES[TileType.BOTTOM_RIGHT.ordinal()] = TOP + LEFT + TOP_LEFT;
        // TODO: 
        TYPE_NEIGHBOUR_VALUES[TileType.INV_TOP_LEFT.ordinal()] = EXCEPT_BOTTOM_RIGHT;
        TYPE_NEIGHBOUR_VALUES[TileType.INV_TOP_RIGHT.ordinal()] = EXCEPT_BOTTOM_LEFT;
        TYPE_NEIGHBOUR_VALUES[TileType.INV_BOTTOM_LEFT.ordinal()] = EXCEPT_TOP_RIGHT;
        TYPE_NEIGHBOUR_VALUES[TileType.INV_BOTTOM_RIGHT.ordinal()] = EXCEPT_TOP_LEFT;

        TYPE_NEIGHBOUR_VALUES[TileType.INV_TOP_RIGHT_BOTTOM_LEFT.ordinal()] = ALL_ADJACENT + BOTTOM_LEFT + TOP_RIGHT;
        TYPE_NEIGHBOUR_VALUES[TileType.INV_BOTTOM_RIGHT_TOP_LEFT.ordinal()] = ALL_ADJACENT + TOP_LEFT + BOTTOM_RIGHT;
    }

    public static TileType evaluate(int u, int v, MapTileComparator comparator) {
        int neighbours = MapNeighbours.evaluate(u, v, comparator);

        for (TileType type : TileType.values()) {
            int typeValue = TYPE_NEIGHBOUR_VALUES[type.ordinal()];
            // Check that pattern is included in neighbours, not just absolute equality
            if ((neighbours & typeValue) == typeValue) {
                return type;
            }
        }

        // if nothing that we recognize, return single
        return TileType.SINGLE;

    }

}
