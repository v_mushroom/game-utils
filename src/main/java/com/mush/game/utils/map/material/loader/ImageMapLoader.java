/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map.material.loader;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mush.game.utils.map.FixedSizeMapContent;
import com.mush.game.utils.map.material.MapMaterial;
import com.mush.game.utils.map.material.MapMaterials;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author mush
 */
public class ImageMapLoader {

    public static class Configuration {

        public Map<String, String> materials;
        public String map;
        public List<String> maps;
    }

    public static FixedSizeMapContent<MapMaterial> loadFromFile(String filePath, MapMaterials materials) throws IOException {
        return loadListFromFile(filePath, materials).get(0);
    }

    public static List<FixedSizeMapContent<MapMaterial>> loadListFromFile(String filePath, MapMaterials materials) throws IOException {
        Logger.getGlobal().log(Level.INFO, "Loading material map from [{0}]", filePath);

        Configuration config = loadJson(filePath);

        Map<Integer, MapMaterial> materialMap = new HashMap<>();

        config.materials.forEach((String materialName, String hexColor) -> {
            Integer materialColor = Integer.parseInt(hexColor, 16);
            MapMaterial material = materials.getMaterialByName(materialName);
            materialMap.put(materialColor, material);
        });

        List<FixedSizeMapContent<MapMaterial>> mapList = new ArrayList<>();
        if (config.maps != null) {
            for (String mapPath : config.maps) {
                mapList.add(loadOneMap(mapPath, materialMap));
            }
        } else if (config.map != null) {
            mapList.add(loadOneMap(config.map, materialMap));
        }

        return mapList;
    }

    private static FixedSizeMapContent<MapMaterial> loadOneMap(String mapPath, Map<Integer, MapMaterial> materialMap) throws IOException {
        BufferedImage image = ImageIO.read(new File(mapPath));

        FixedSizeMapContent<MapMaterial> map = new FixedSizeMapContent<>(image.getWidth(), image.getHeight());

        Logger.getGlobal().log(Level.INFO, "Reading pixels");
        for (int v = 0; v < map.getHeight(); v++) {
            for (int u = 0; u < map.getWidth(); u++) {
                int rgb = image.getRGB(u, v) & 0xffffff;
                MapMaterial material = materialMap.get(rgb);
                if (material != null) {
                    map.setItemAt(u, v, material);
                }
            }
        }

        return map;
    }

    private static Configuration loadJson(String fileName) throws IOException {
        File jsonFile = new File(fileName);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Configuration configuration = mapper.readValue(jsonFile, Configuration.class);
        return configuration;
    }

}
