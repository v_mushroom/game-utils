/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map;

import java.util.Objects;

/**
 *
 * @author mush
 */
public class MapContentComparator<T> implements MapTileComparator {

    private MapContent<T> mapContent;
    
//    public MapContentComparator(MapContent<T> mapContent) {
//        this.mapContent = mapContent;
//    }
    
    public void setMapContent(MapContent<T> mapContent) {
        this.mapContent = mapContent;
    }

    @Override
    public boolean isSame(int u, int v, int uOfs, int vOfs) {
        if (mapContent == null) {
            return true;
        }
        T here = mapContent.getItemAt(u, v);
        T there = mapContent.getItemAt(u + uOfs, v + vOfs);
        return isSame(here, there);
    }
    
    public boolean isSame(T here, T there) {
        return Objects.equals(here, there);
    }

}
