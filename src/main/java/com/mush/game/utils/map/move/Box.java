/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map.move;

import java.awt.geom.Point2D;

/**
 * Basically the same as Rectangle2D.Double, but with different field names.
 *
 * @author mush
 */
public class Box {

    public double left;
    public double right;
    public double top;
    public double bottom;

    public Box() {
        set(0, 0, 0, 0);
    }

    public Box(double left, double top, double right, double bottom) {
        set(left, top, right, bottom);
    }

    public Box(Box box) {
        set(box);
    }

    public void set(double left, double top, double right, double bottom) {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
    }

    public void set(Box box) {
        set(box.left, box.top, box.right, box.bottom);
    }

    /**
     * A bounding box has positive values originating from its center
     *
     * @param bounds e.g. (0.5, 0.5, 0.5, 0.5)
     * @param x
     * @param y
     */
    public void recalculateFromBoundsAndPosition(Box bounds, double x, double y) {
        if (bounds != null) {
            set(
                    x - bounds.left,
                    y - bounds.top,
                    x + bounds.right,
                    y + bounds.bottom
            );
        }
    }

    public void offsetBy(double dx, double dy) {
        set(left + dx, top + dy, right + dx, bottom + dy);
    }

    public Point2D.Double getCenter() {
        return new Point2D.Double((left + right) / 2, (top + bottom) / 2);
    }

    @Override
    public String toString() {
        return "Box (" + left + ", " + top + ", " + right + ", " + bottom + ")";
    }

}
