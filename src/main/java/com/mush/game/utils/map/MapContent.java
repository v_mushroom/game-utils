/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map;

/**
 *
 * @author mush
 */
public interface MapContent<T> {
    
    public T getItemAt(int u, int v);
    
    public void setItemAt(int u, int v, T item);
}
