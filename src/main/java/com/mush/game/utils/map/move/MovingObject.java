/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map.move;

import com.mush.game.utils.sprites.Sprite;
import java.awt.geom.Point2D;

/**
 *
 * @author mush
 */
public class MovingObject {

    /**
     * Id assigned externally
     */
    public Integer id;
    public Point2D.Double position;
    /**
     * Movement used each frame for collision detection
     */
    public Point2D.Double movement;

    /**
     * Actual desired velocity
     */
    public Point2D.Double velocity;
    public Box bounds;
    public Box movingBox;
    
    public boolean collidesWithMobs = true;
    public boolean collidesWithMap = true;
    public boolean isObstacleForMobs = true;
    
    public Sprite sprite;

    public MovingObject() {
        id = null;
        bounds = new Box(-1, -1, 1, 1);
        position = new Point2D.Double();
        movement = new Point2D.Double();
        velocity = new Point2D.Double();
        movingBox = new Box();
    }

    public void updateMovingBox() {
        movingBox.recalculateFromBoundsAndPosition(
                bounds, position.getX(), position.getY());
    }

    public void prepareMovement(double elapsedSeconds) {
        movement.setLocation(
                velocity.getX() * elapsedSeconds,
                velocity.getY() * elapsedSeconds);
    }

    public boolean hasMovement() {
        return !(movement.getX() == 0 && movement.getY() == 0);
    }

    public void applyMovementToPosition() {
        position.setLocation(
                position.getX() + movement.getX(),
                position.getY() + movement.getY());
    }
}
