/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map.move;

import com.mush.game.utils.map.MapContent;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author mush
 */
public class MapContentCollision<T> {

    public static class MapItemCollision<T> {

        public T mapItem;
        public int u;
        public int v;
    }

    private final MapContentItemDimension itemDimension;
    private MapContent<T> mapContent;
    private final MapContentObstacleEvaluator<T> obstacleEvaluator;

    public MapContentCollision(
            MapContentItemDimension itemDimension,
            MapContentObstacleEvaluator<T> obstacleEvaluator) {
        this.itemDimension = itemDimension;
        this.obstacleEvaluator = obstacleEvaluator;
    }
    
    public void setMapContent(MapContent<T> mapContent) {
        this.mapContent = mapContent;
    }

    public T checkMapItemStaticCollision(int u, int v, Box movingBox, Box reuse) {
        return checkMapItemMovementCollision(u, v, movingBox, reuse, null);
    }

    public T checkMapItemMovementCollision(int u, int v, Box movingBox, Box reuse, Point2D.Double movement) {
        if (mapContent == null) {
            return null;
        }
        
        T mapItem = mapContent.getItemAt(u, v);

        if (obstacleEvaluator.isObstacle(mapItem)) {
            Box mapItemBox = itemDimension.getBoxAtUv(u, v, reuse);
            if (BoxCollision.checkMoveCollision(movingBox, mapItemBox, movement)) {
                return mapItem;
            }
        }
        return null;
    }

    public Set<MapItemCollision<T>> checkMapItemMovementCollisions(Box movingBox, Point2D.Double movement) {
        Box reuse = new Box(movingBox);

        // Original box before movement
        reuse.offsetBy(-movement.x, -movement.y);

        Point originalCenterUv = itemDimension.getUvAtRealCoordinates(reuse.getCenter());
        Set<MapItemCollision<T>> collidedItems = new HashSet<>();

        // First check centre
        checkAdjacentMapItemMovementCollision(originalCenterUv, 0, 0, movingBox, reuse, movement, collidedItems);

        // Then check direct neighbours
        checkAdjacentMapItemMovementCollision(originalCenterUv, -1, 0, movingBox, reuse, movement, collidedItems);
        checkAdjacentMapItemMovementCollision(originalCenterUv, 0, -1, movingBox, reuse, movement, collidedItems);
        checkAdjacentMapItemMovementCollision(originalCenterUv, +1, 0, movingBox, reuse, movement, collidedItems);
        checkAdjacentMapItemMovementCollision(originalCenterUv, 0, +1, movingBox, reuse, movement, collidedItems);

        // Then check diagonal neighbours
        checkAdjacentMapItemMovementCollision(originalCenterUv, -1, -1, movingBox, reuse, movement, collidedItems);
        checkAdjacentMapItemMovementCollision(originalCenterUv, +1, -1, movingBox, reuse, movement, collidedItems);
        checkAdjacentMapItemMovementCollision(originalCenterUv, +1, +1, movingBox, reuse, movement, collidedItems);
        checkAdjacentMapItemMovementCollision(originalCenterUv, -1, +1, movingBox, reuse, movement, collidedItems);

        return collidedItems;
    }

    private void checkAdjacentMapItemMovementCollision(Point centerItemUv, int du, int dv, Box movingBox, Box reuse, Point2D.Double movement, Set<MapItemCollision<T>> collidedItems) {
        Point adjacentItemUv = new Point(centerItemUv.x + du, centerItemUv.y + dv);
        double prevMovementX = movement.x;
        double prevMovementY = movement.y;

        T mapItem = checkMapItemMovementCollision(adjacentItemUv.x, adjacentItemUv.y, movingBox, reuse, movement);

        if (mapItem != null) {
            double movementCorrectionX = movement.x - prevMovementX;
            double movementCorrectionY = movement.y - prevMovementY;
            movingBox.offsetBy(movementCorrectionX, movementCorrectionY);
            
            MapItemCollision<T> collision = new MapItemCollision<>();
            collision.mapItem = mapItem;
            collision.u = adjacentItemUv.x;
            collision.v = adjacentItemUv.y;
            collidedItems.add(collision);
        }
    }

}
