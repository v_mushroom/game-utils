/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map.move;

/**
 *
 * @author mush
 */
public interface MapContentObstacleEvaluator<T> {

    public boolean isObstacle(T item);
}
