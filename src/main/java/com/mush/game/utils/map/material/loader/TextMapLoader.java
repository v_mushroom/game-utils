/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map.material.loader;

import com.mush.game.utils.map.FixedSizeMapContent;
import com.mush.game.utils.map.material.MapMaterial;
import com.mush.game.utils.map.material.MapMaterials;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 *
 * @author mush
 */
public class TextMapLoader {

    public static FixedSizeMapContent<MapMaterial> loadFromFile(
            String filePath,
            MapMaterials materials
    ) throws IOException {

        Logger.getGlobal().log(Level.INFO, "Loading material map from [{0}]", filePath);

        Path path = Paths.get(filePath);
        List<String> mapLines = new ArrayList<>();
        Pattern pattern = Pattern.compile("([^ =]) = ([^ =]+)");
        AtomicInteger width = new AtomicInteger(0);
        Map<Character, Integer> materialIdMap = new HashMap<>();

        Stream<String> lines = Files.lines(path);
        lines.forEach((line) -> {
            if (line.startsWith("+")) {
                width.set(line.length() - 1);
            } else if (line.startsWith("|")) {
                String trimLine = (line.length() > width.get() + 1)
                        ? line.substring(1, width.get() + 1)
                        : line.substring(1);
                mapLines.add(trimLine);
            } else {
                Matcher matcher = pattern.matcher(line);
                if (matcher.find()) {
                    String materialChar = matcher.group(1);
                    String materialName = matcher.group(2);
                    MapMaterial material = materials.getMaterialByName(materialName);
                    if (material != null) {
                        materialIdMap.put(materialChar.charAt(0), material.id);
                    }
                }
            }
        });

        Logger logger = Logger.getGlobal();

        logger.log(Level.INFO, "{0}", materialIdMap);
        logger.log(Level.INFO, "w:{0} h:{1}", new Object[]{width.get(), mapLines.size()});

        mapLines.forEach((t) -> {
            logger.log(Level.INFO, "map:[{0}]", t);
        });

        FixedSizeMapContent<MapMaterial> mapContent = new FixedSizeMapContent<>(width.get(), mapLines.size());
        mapContent.clear(null);

        int w = width.get();
        int h = mapLines.size();

        for (int v = 0; v < h; v++) {
            String mapLine = mapLines.get(v);
            for (int u = 0; u < w; u++) {
                if (u < mapLine.length()) {
                    char c = mapLine.charAt(u);
                    Integer matId = materialIdMap.get(c);
                    MapMaterial material = materials.getMaterialById(matId);
                    mapContent.setItemAt(u, v, material);
                }
            }
        }

        return mapContent;
    }

    private static Character getCharForMaterial(
            MapMaterial material,
            Map<Integer, Character> charMap) {

        Character character = charMap.get(material.id);
        if (character == null) {

            char c = material.name.charAt(0);
            boolean found = false;

            while (c < 250 && !found) {
                if (charMap.containsValue(c)) {
                    c++;
                } else {
                    found = true;
                }
            }

            character = c;
            charMap.put(material.id, c);
        }

        return character;
    }

    public static List<String> output(FixedSizeMapContent<MapMaterial> content, MapMaterials materials) {
        List<String> lines = new ArrayList<>();
        Map<Integer, Character> charMap = new HashMap<>();

        Set<String> names = materials.getMaterialNames();
        names.forEach((name) -> {
            MapMaterial material = materials.getMaterialByName(name);
            Character character = getCharForMaterial(material, charMap);
            lines.add(character + " = " + name);
        });

        lines.add("");

        StringBuilder sb = new StringBuilder("+");
        for (int i = 0; i < content.getWidth(); i++) {
            sb.append("-");
        }
        lines.add(sb.toString());

        for (int v = 0; v < content.getHeight(); v++) {
            sb = new StringBuilder("|");
            for (int u = 0; u < content.getWidth(); u++) {
                MapMaterial item = content.getItemAt(u, v);
                if (item != null) {
                    Character character = getCharForMaterial(item, charMap);
                    sb.append(character);
                } else {
                    sb.append(' ');
                }
            }
            lines.add(sb.toString());
        }

        return lines;
    }

}
