/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map;

/**
 *
 * @author mush
 */
public class MapWall {

    public enum TileType {
        // + junction
        UP_DOWN_LEFT_RIGHT,
        // T junction
        LEFT_RIGHT_UP,
        LEFT_RIGHT_DOWN,
        UP_DOWN_LEFT,
        UP_DOWN_RIGHT,
        // Straight segment
        UP_DOWN,
        LEFT_RIGHT,
        // Corner
        UP_RIGHT,
        RIGHT_DOWN,
        DOWN_LEFT,
        LEFT_UP,
        // One-sided end
        UP,
        DOWN,
        LEFT,
        RIGHT,
        // Just one tile
        SINGLE,
    }

    private static final int[] TYPE_NEIGHBOUR_VALUES;

    static {
        TYPE_NEIGHBOUR_VALUES = new int[TileType.values().length];

        TYPE_NEIGHBOUR_VALUES[TileType.SINGLE.ordinal()] = 0;

        TYPE_NEIGHBOUR_VALUES[TileType.UP.ordinal()] = MapNeighbours.TOP;
        TYPE_NEIGHBOUR_VALUES[TileType.DOWN.ordinal()] = MapNeighbours.BOTTOM;
        TYPE_NEIGHBOUR_VALUES[TileType.LEFT.ordinal()] = MapNeighbours.LEFT;
        TYPE_NEIGHBOUR_VALUES[TileType.RIGHT.ordinal()] = MapNeighbours.RIGHT;

        TYPE_NEIGHBOUR_VALUES[TileType.UP_DOWN.ordinal()] = MapNeighbours.TOP + MapNeighbours.BOTTOM;
        TYPE_NEIGHBOUR_VALUES[TileType.LEFT_RIGHT.ordinal()] = MapNeighbours.LEFT + MapNeighbours.RIGHT;

        TYPE_NEIGHBOUR_VALUES[TileType.UP_RIGHT.ordinal()] = MapNeighbours.TOP + MapNeighbours.RIGHT;
        TYPE_NEIGHBOUR_VALUES[TileType.RIGHT_DOWN.ordinal()] = MapNeighbours.BOTTOM + MapNeighbours.RIGHT;
        TYPE_NEIGHBOUR_VALUES[TileType.DOWN_LEFT.ordinal()] = MapNeighbours.BOTTOM + MapNeighbours.LEFT;
        TYPE_NEIGHBOUR_VALUES[TileType.LEFT_UP.ordinal()] = MapNeighbours.TOP + MapNeighbours.LEFT;

        TYPE_NEIGHBOUR_VALUES[TileType.LEFT_RIGHT_UP.ordinal()] = MapNeighbours.LEFT + MapNeighbours.RIGHT + MapNeighbours.TOP;
        TYPE_NEIGHBOUR_VALUES[TileType.LEFT_RIGHT_DOWN.ordinal()] = MapNeighbours.LEFT + MapNeighbours.RIGHT + MapNeighbours.BOTTOM;
        TYPE_NEIGHBOUR_VALUES[TileType.UP_DOWN_LEFT.ordinal()] = MapNeighbours.TOP + MapNeighbours.BOTTOM + MapNeighbours.LEFT;
        TYPE_NEIGHBOUR_VALUES[TileType.UP_DOWN_RIGHT.ordinal()] = MapNeighbours.TOP + MapNeighbours.BOTTOM + MapNeighbours.RIGHT;

        TYPE_NEIGHBOUR_VALUES[TileType.UP_DOWN_LEFT_RIGHT.ordinal()] = MapNeighbours.TOP + MapNeighbours.BOTTOM + MapNeighbours.LEFT + MapNeighbours.RIGHT;
    }

    public static TileType evaluate(int u, int v, MapTileComparator comparator) {
        int neighbours = MapNeighbours.evaluate(u, v, comparator);

        for (TileType type : TileType.values()) {
            int typeValue = TYPE_NEIGHBOUR_VALUES[type.ordinal()];
            // Check that pattern is included in neighbours, not just absolute equality
            if ((neighbours & typeValue) == typeValue) {
                return type;
            }
        }

        // if nothing that we recognize, return single
        return TileType.SINGLE;
    }
}
