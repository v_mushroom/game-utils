/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map.material;

import java.util.Map;

/**
 *
 * @author mush
 */
public class MapMaterial {

    public final Integer id;
    public final String name;
    public final Map<String, Object> attributes;
    // map attributes

    MapMaterial(Integer id, String name, Map<String, Object> attributes) {
        this.id = id;
        this.name = name;
        this.attributes = attributes;
    }
//    id, name, attributes
//            compare
//            
//            collected in MapMaterials
//                    load from json
//                            
//    map<map material> loader from txt

}
