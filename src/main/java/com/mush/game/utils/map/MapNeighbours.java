/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map;

/**
 *
 * @author mush
 */
public class MapNeighbours {

    /*
    * 128 1 2
    * 64  0 4
    * 32 16 8
     */
    public static final int TOP = 1;
    public static final int TOP_RIGHT = 2;
    public static final int RIGHT = 4;
    public static final int BOTTOM_RIGHT = 8;
    public static final int BOTTOM = 16;
    public static final int BOTTOM_LEFT = 32;
    public static final int LEFT = 64;
    public static final int TOP_LEFT = 128;
    
    public static final int ALL_ADJACENT = TOP + RIGHT + BOTTOM + LEFT;
    public static final int ALL_DIAGONAL = TOP_RIGHT + BOTTOM_RIGHT + BOTTOM_LEFT + TOP_LEFT;
    
    public static final int ALL = ALL_ADJACENT + ALL_DIAGONAL;
    
    public static final int EXCEPT_TOP_LEFT = ALL - TOP_LEFT;
    public static final int EXCEPT_TOP_RIGHT = ALL - TOP_RIGHT;
    public static final int EXCEPT_BOTTOM_RIGHT = ALL - BOTTOM_RIGHT;
    public static final int EXCEPT_BOTTOM_LEFT = ALL - BOTTOM_LEFT;
    
    public static final int ALL_LEFT = LEFT + TOP_LEFT + BOTTOM_LEFT;
    public static final int ALL_RIGHT = RIGHT + TOP_RIGHT + BOTTOM_RIGHT;
    public static final int ALL_TOP = TOP + TOP_LEFT + TOP_RIGHT;
    public static final int ALL_BOTTOM = BOTTOM + BOTTOM_LEFT + BOTTOM_RIGHT;

    public static int evaluate(int u, int v, MapTileComparator comparator) {
        int value = 0;

        value = evaluate(u, v, +0, -1, comparator, TOP, value);
        value = evaluate(u, v, +1, -1, comparator, TOP_RIGHT, value);
        value = evaluate(u, v, +1, +0, comparator, RIGHT, value);
        value = evaluate(u, v, +1, +1, comparator, BOTTOM_RIGHT, value);
        value = evaluate(u, v, +0, +1, comparator, BOTTOM, value);
        value = evaluate(u, v, -1, +1, comparator, BOTTOM_LEFT, value);
        value = evaluate(u, v, -1, +0, comparator, LEFT, value);
        value = evaluate(u, v, -1, -1, comparator, TOP_LEFT, value);

        return value;
    }

    private static int evaluate(int u, int v, int uOfs, int vOfs, MapTileComparator comparator, int value, int totalValue) {
        if (comparator.isSame(u, v, uOfs, vOfs)) {
            return totalValue + value;
        }
        return totalValue;
    }
}
