/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.render;

import java.awt.Graphics2D;

/**
 *
 * @author mush
 */
public interface Renderable {
    
    public void renderContent(Graphics2D g, GameRenderer renderer);
}
