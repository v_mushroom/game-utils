/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.render;

/**
 *
 * @author mush
 */
public class TrackingCamera extends CenteredCamera {

    private double speed = 1;
    private double maxDiff;

    public TrackingCamera(int width, int height) {
        super(width, height);
        maxDiff = (width > height ? height : width) / 2;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void setTarget(double x, double y, double vx, double vy) {
        double v = Math.sqrt(vx * vx + vy * vy);
        if (v < 1) {
            v = 1;
        }
        target.setLocation(x + 10 * vx / v, y + 10 * vy / v);
    }

    @Override
    public void update(double elapsedSeconds) {
        double xDiff = getXDiff();
        double yDiff = getYDiff();

        double diff = Math.sqrt(xDiff * xDiff + yDiff * yDiff);

        double diffFact = 1 * diff / maxDiff;
        if (diffFact > 1) {
            diffFact = 1;
        }
        double timeFact = Math.pow(elapsedSeconds * speed, 1 - diffFact);

        offset.setLocation(offset.getX() + xDiff * timeFact, offset.getY() + yDiff * timeFact);
    }

}
