/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.render;

import com.mush.game.utils.tiles.TiledSurface;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mush
 */
public class SimpleLayeredRenderer extends GameRenderer {
    
    private List<TiledSurface> layers;
    
    public SimpleLayeredRenderer(int width, int height) {
        super(width, height);
        layers = new ArrayList<>();
    }
    
    public void clear() {
        layers.clear();
    }
    
    public void addTiledLayer(TiledSurface layer) {
        layers.add(layer);
    }

    @Override
    public void renderContent(Graphics2D g) {
        layers.forEach((layer) -> {
            layer.draw(g);
        });
    }
    
}
