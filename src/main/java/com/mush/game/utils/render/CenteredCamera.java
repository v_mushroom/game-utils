/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.render;

import java.awt.geom.Point2D;

/**
 *
 * @author mush
 */
public class CenteredCamera {

    protected int viewWidth;
    protected int viewHeight;
    protected Point2D.Double offset;
    protected Point2D.Double target;

    protected Double minX = null;
    protected Double maxX = null;
    protected Double minY = null;
    protected Double maxY = null;

    public CenteredCamera(int width, int height) {
        viewWidth = width;
        viewHeight = height;
        offset = new Point2D.Double();
        target = new Point2D.Double();
    }

    public void setXBounds(double minX, double maxX) {
        this.minX = minX;
        this.maxX = maxX;
    }

    public void setYBounds(double minY, double maxY) {
        this.minY = minY;
        this.maxY = maxY;
    }

    public void setTarget(double x, double y) {
        target.setLocation(x, y);
    }

    public void setPosition(double x, double y) {
        offset.setLocation(x, y);
    }

    public void update(double elapsedSeconds) {
        offset.setLocation(offset.getX() + getXDiff(), offset.getY() + getYDiff());
    }

    public double getxOffset() {
        return offset.getX();
    }

    public double getyOffset() {
        return offset.getY();
    }

    protected double getXDiff() {
        double xCenter = offset.getX() + viewWidth / 2;
        double xDiff = target.getX() - xCenter;
        if (minX != null && offset.getX() + xDiff < minX) {
            xDiff = minX - offset.getX();
        }
        if (maxX != null && offset.getX() + xDiff > maxX) {
            xDiff = maxX - offset.getX();
        }
        return xDiff;
    }

    protected double getYDiff() {
        double yCenter = offset.getY() + viewHeight / 2;
        double yDiff = target.getY() - yCenter;
        if (minY != null && offset.getY() + yDiff < minY) {
            yDiff = minY - offset.getY();
        }
        if (maxY != null && offset.getY() + yDiff > maxY) {
            yDiff = maxY - offset.getY();
        }
        return yDiff;
    }

}
