/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.render;

import java.awt.geom.Point2D;

/**
 *
 * @author mush
 */
public class TrailingTargetCamera extends CenteredCamera {

    private Point2D.Double leadingTarget;
    private double leadingRange = 0;

    public TrailingTargetCamera(int width, int height) {
        super(width, height);
        leadingTarget = new Point2D.Double();
    }

    public void setLeadingRange(double range) {
        leadingRange = range;
    }

    @Override
    public void setPosition(double x, double y) {
        super.setPosition(x, y);
        leadingTarget.setLocation(x, y);
    }

    @Override
    public void setTarget(double x, double y) {
        leadingTarget.setLocation(x, y);
    }

    @Override
    public void update(double elapsedSeconds) {
        double leadingDistance = leadingTarget.distance(target);
        if (leadingDistance > leadingRange) {
            double remainingDistance = leadingDistance - leadingRange;
            Point2D.Double diff = vectorTo(target, leadingTarget);
            normalizeTo(remainingDistance, diff);
            target.setLocation(target.getX() + diff.getX(), target.getY() + diff.getY());
        }

        super.update(elapsedSeconds);
    }

    private Point2D.Double vectorTo(Point2D.Double a, Point2D.Double b) {
        return new Point2D.Double(b.getX() - a.getX(), b.getY() - a.getY());
    }

    private void normalizeTo(double length, Point2D.Double v) {
        double vLen = v.distance(0, 0);
        if (vLen > 0) {
            double factor = length / vLen;
            v.setLocation(v.getX() * factor, v.getY() * factor);
        }
    }

}
