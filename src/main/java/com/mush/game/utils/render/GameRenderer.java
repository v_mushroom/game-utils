/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.render;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mush
 */
public abstract class GameRenderer {

    private BufferedImage screenImage;
    private Graphics2D screenGraphics;
    private BufferedImage bufferImage;
    private Graphics2D bufferGraphics;
    private int screenWidth;
    private int screenHeight;
    private double pixelScale = 1;
    private double fps;
    private final int fpsHistoryLength = 50;
    private double[] fpsHistory = new double[fpsHistoryLength];
    private int fpsHistoryIndex = 0;
    private boolean showFps = false;
    private boolean showFpsHistory = false;
    private boolean useBuffer = false;
    private Map<RenderingHints.Key, Object> renderingHintsMap;

    public GameRenderer(int width, int height) {
        renderingHintsMap = new HashMap<>();
        setScreenSize(width, height);
    }

    public GameRenderer(int width, int height, int scale) {
        this.screenWidth = width;
        this.screenHeight = height;
        this.pixelScale = scale;
        renderingHintsMap = new HashMap<>();

        createScreenImage();
    }

    public BufferedImage getScreenImage() {
        return screenImage;
    }

    public boolean getShowFps() {
        return showFps;
    }

    public double getFps() {
        return fps;
    }

    public final void setScreenSize(int width, int height) {
        this.screenWidth = width;
        this.screenHeight = height;

        createScreenImage();
    }

    public final void setPixelScale(double scale) {
        this.pixelScale = scale;
    }

    public final void setUseBuffer(boolean v) {
        useBuffer = true;
        createScreenImage();
    }

    private void createScreenImage() {
        if (screenGraphics != null) {
            screenGraphics.dispose();
            screenGraphics = null;
            screenImage = null;
        }

        screenImage = new BufferedImage(
                (int) Math.ceil(screenWidth / pixelScale),
                (int) Math.ceil(screenHeight / pixelScale),
                BufferedImage.TYPE_INT_RGB);

        screenGraphics = screenImage.createGraphics();

        if (useBuffer) {
            createBufferImage();
        } else {
            destroyBufferImage();
        }

        applyRenderingHints();

        Logger logger = Logger.getGlobal();
        logger.log(Level.INFO, "Screen size: {0} {1}", new Object[]{screenWidth, screenHeight});
        logger.log(Level.INFO, "Image size: {0} {1}", new Object[]{screenImage.getWidth(), screenImage.getHeight()});
    }

    private void destroyBufferImage() {
        if (bufferGraphics != null) {
            bufferGraphics.dispose();
            bufferGraphics = null;
            bufferImage = null;
        }
    }

    private void createBufferImage() {
        destroyBufferImage();

        bufferImage = new BufferedImage(
                screenImage.getWidth(),
                screenImage.getHeight(),
                BufferedImage.TYPE_INT_RGB);

        bufferGraphics = bufferImage.createGraphics();
    }

    private void swapBuffer() {
        if (bufferImage == null) {
            return;
        }

        BufferedImage tempImage = screenImage;
        Graphics2D tempGraphics = screenGraphics;

        screenImage = bufferImage;
        screenGraphics = bufferGraphics;

        bufferImage = tempImage;
        bufferGraphics = tempGraphics;
    }

    public void setRenderingHint(RenderingHints.Key hintKey, Object hintValue) {
        renderingHintsMap.put(hintKey, hintValue);
    }

    public void applyRenderingHints() {
        for (Map.Entry<RenderingHints.Key, Object> hint : renderingHintsMap.entrySet()) {
            screenGraphics.setRenderingHint(hint.getKey(), hint.getValue());
        }
    }

    public final void render() {
        if (useBuffer) {
            renderContent(bufferGraphics);
            swapBuffer();
        } else {
            renderContent(screenGraphics);
        }
    }

    public final void draw(Graphics2D g) {
        AffineTransform t = null;
        if (pixelScale > 1) {
            t = g.getTransform();
            g.scale(pixelScale, pixelScale);
        }
        g.drawImage(screenImage, 0, 0, null);

        if (t != null) {
            g.setTransform(t);
        }

        if (showFps) {
            if (showFpsHistory) {
                drawFpsHistory(g);
            } else {
                drawFps(g, fps);
            }
        }
    }

//    public final WritableImage draw(GraphicsContext gc, WritableImage image0) {
//        WritableImage image = SwingFXUtils.toFXImage(screenImage, image0);
//        gc.drawImage(image, 0, 0);
//
//        if (showFps) {
//            gc.setLineWidth(1);
//            gc.setFill(javafx.scene.paint.Color.LIMEGREEN);
//            gc.fillText("" + Math.round(fps), 1, 13);
//        }
//
//        return image;
//    }
    public abstract void renderContent(Graphics2D g);

    public int getScreenWidth() {
        return screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }

    public void showFps(boolean show) {
        this.showFps = show;
    }

    public void showFpsHistory(boolean show) {
        this.showFpsHistory = show;
    }

    public void upadteFps(double fps) {
        this.fps = fps;
        this.fpsHistory[this.fpsHistoryIndex] = fps;
        this.fpsHistoryIndex = (this.fpsHistoryIndex + 1) % this.fpsHistoryLength;
    }

    private void drawFpsHistory(Graphics2D g) {
        double min = 100000;
        double max = 0;
        double total = 0;
        for (double sample : fpsHistory) {
            if (sample < min) {
                min = sample;
            }
            if (sample > max) {
                max = sample;
            }
            total += sample;
        }
        double avg = total / fpsHistoryLength;
        double range = max - min;
        if (range < 1) {
            range = 1;
        }

        int height = 10;
        int x0 = 0;
        int y0 = 30 + height;

        g.setPaint(Color.GREEN);
        for (int i = 0; i < fpsHistoryLength; i++) {
            double v = height * (fpsHistory[i] - min) / range;
            g.drawLine(x0 + i, y0, x0 + i, (int) (y0 - v));
        }

        g.drawString("" + Math.round(min), x0, y0 + 13);
        g.drawString("" + Math.round(max), x0, y0 - 13);

        drawFps(g, avg);
    }

    private void drawFps(Graphics2D g, double v) {
        g.setPaint(Color.BLACK);
        g.drawString("" + Math.round(v), 1, 13);
        g.setPaint(Color.GREEN);
        g.drawString("" + Math.round(v), 0, 12);
    }

}
