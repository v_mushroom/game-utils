/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.render;

import java.awt.Graphics2D;

/**
 * Instead of extending GameRenderer, you can use a single renderer and swap renderables
 * 
 * @author mush
 */
public class DelegatingGameRenderer extends GameRenderer {
    
    private Renderable renderable;

    public DelegatingGameRenderer(int width, int height) {
        super(width, height);
    }

    public DelegatingGameRenderer(int width, int height, int scale) {
        super(width, height, scale);
    }
    
    public DelegatingGameRenderer(int width, int height, Renderable renderable) {
        super(width, height);
        setRenderable(renderable);
    }

    public DelegatingGameRenderer(int width, int height, int scale, Renderable renderable) {
        super(width, height, scale);
        setRenderable(renderable);
    }
    
    public void setRenderable(Renderable renderable) {
        this.renderable = renderable;
    }

    @Override
    public void renderContent(Graphics2D g) {
        if (renderable != null) {
            renderable.renderContent(g, this);
        }
    }

}
