/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.tiles;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mush.game.utils.map.MapContentComparator;
import com.mush.game.utils.map.material.MapMaterial;
import com.mush.game.utils.map.material.MapMaterials;
import com.mush.game.utils.sprites.SpriteAnimation;
import com.mush.game.utils.sprites.SpriteFactory;
import com.mush.game.utils.sprites.SpriteSheet;
import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mush
 */
public class MaterialDataSourceLoader {

    public static class Schema {

        public static class MaterialTiles {

            public static class Size {

                public Integer width = 1;
                public Integer height = 1;
            }

            public static class Material {

                public String tileSheet;
                public String randomTileSheet;
                public Integer randomTiles;
                public String wallTileSheet;
                public String boundaryTileSheet;
                public Integer u = 0;
                public Integer v = 0;
                public String base;
            }

            public Map<String, Material> materials;
            public Size size;
        }

        public MaterialTiles materialTiles;
    }

    public static MapContentSurfaceDataSource<MapMaterial> loadFromFile(
            String filePath,
            MapMaterials materials/*,
            MapContentSurfaceDataSource<MapMaterial> materialDataSource*/
    ) throws IOException {
        return loadFromFile(filePath, materials, null);
    }

    public static MapContentSurfaceDataSource<MapMaterial> loadFromFile(
            String filePath,
            MapMaterials materials,
            MapContentComparator<MapMaterial> mapContentComparator
    ) throws IOException {
        Schema schema = loadJson(filePath);
        if (schema.materialTiles == null) {
            Logger.getGlobal().log(Level.WARNING, "schema.materialTiles null");
            return null;
        }

        Schema.MaterialTiles config = schema.materialTiles;
        Schema.MaterialTiles.Size tileSize = config.size != null
                ? config.size
                : new Schema.MaterialTiles.Size();

        MapContentSurfaceDataSource<MapMaterial> materialDataSource = mapContentComparator == null
                ? new MapContentSurfaceDataSource<>(tileSize.width, tileSize.height)
                : new MapContentSurfaceDataSource<>(tileSize.width, tileSize.height, mapContentComparator);

        if (config.materials == null) {
            Logger.getGlobal().log(Level.WARNING, "schema.materialTiles.materials null");
            return materialDataSource;
        }

        SpriteFactory spriteFactory = new SpriteFactory();

        config.materials.forEach((materialName, materialConfig) -> {
//            System.out.println(materialName);
            MapMaterial material = materials.getMaterialByName(cleanupMaterialName(materialName));
            MapMaterial baseMaterial = null;
            if (materialConfig.base != null) {
                baseMaterial = materials.getMaterialByName(cleanupMaterialName(materialConfig.base));
            }
            if (material != null) {
                try {
                    setMaterialTile(material, baseMaterial, materialConfig, tileSize, spriteFactory, materialDataSource);
                } catch (IOException e) {
                    Logger.getGlobal().log(Level.WARNING, "Error setting material", e);
                }
            }
        });

        return materialDataSource;
    }

    /**
     * Can name material configurations MATERIAL:something to have multiple
     * configurations
     *
     * @param fullMaterialName
     * @return
     */
    private static String cleanupMaterialName(String fullMaterialName) {
        String[] parts = fullMaterialName.split(":");
        if (parts.length > 1) {
            return parts[0];
        }
        return fullMaterialName;
    }

    private static void setMaterialTile(
            MapMaterial material,
            MapMaterial baseMaterial,
            Schema.MaterialTiles.Material config,
            Schema.MaterialTiles.Size size,
            SpriteFactory spriteFactory,
            MapContentSurfaceDataSource<MapMaterial> dataSource) throws IOException {

        BufferedImage baseMaterialTile = null;
        if (baseMaterial != null) {
            SpriteAnimation baseMaterialAnimation = dataSource.getAnimationForMapTile(baseMaterial);
            if (baseMaterialAnimation != null) {
                baseMaterialTile = baseMaterialAnimation.getFrame(0);
            }
        }

        if (config.tileSheet != null) {
            setMaterialSingleTile(material, config, size, dataSource, spriteFactory, baseMaterialTile);
        }
        if (config.randomTileSheet != null) {
            setMaterialRandomSingleTile(material, config, size, dataSource, spriteFactory, baseMaterialTile);
        }
        if (config.wallTileSheet != null) {
            setMaterialWallTile(material, config, size, dataSource, spriteFactory, baseMaterialTile);
        }
        if (config.boundaryTileSheet != null) {
            setMaterialBoundaryTile(material, config, size, dataSource, spriteFactory, baseMaterialTile);
        }
    }

    private static SpriteAnimation createSingleTile(
            Schema.MaterialTiles.Material config,
            String tileSheet,
            Schema.MaterialTiles.Size size,
            int u,
            int v,
            SpriteFactory spriteFactory,
            BufferedImage baseMaterialTile) throws IOException {

        SpriteSheet spriteSheet = spriteFactory.getSpriteSheet(tileSheet, size.width, size.height);

        BufferedImage spriteImage = spriteSheet.cutImage(u, v);
        if (baseMaterialTile != null) {
            Graphics2D g = (Graphics2D) spriteImage.getGraphics();
            g.setComposite(AlphaComposite.DstOver);
            g.drawImage(baseMaterialTile, null, 0, 0);
            g.dispose();
        }

        SpriteAnimation animation = new SpriteAnimation(spriteImage);

        return animation;
    }

    private static void setMaterialSingleTile(
            MapMaterial material,
            Schema.MaterialTiles.Material config,
            Schema.MaterialTiles.Size size,
            MapContentSurfaceDataSource<MapMaterial> dataSource,
            SpriteFactory spriteFactory,
            BufferedImage baseMaterialTile) throws IOException {

        SpriteAnimation animation = createSingleTile(config, config.tileSheet, size, config.u, config.v, spriteFactory, baseMaterialTile);

        dataSource.setAnimationForMapTile(material, animation);
    }

    private static void setMaterialRandomSingleTile(
            MapMaterial material,
            Schema.MaterialTiles.Material config,
            Schema.MaterialTiles.Size size,
            MapContentSurfaceDataSource<MapMaterial> dataSource,
            SpriteFactory spriteFactory,
            BufferedImage baseMaterialTile) throws IOException {

        int tileCount = config.randomTiles != null ? config.randomTiles : 1;

        for (int i = 0; i < tileCount; i++) {
            SpriteAnimation animation = createSingleTile(config, config.randomTileSheet, size, config.u + i, config.v, spriteFactory, baseMaterialTile);
            dataSource.setAnimationForRandomMapTile(material, animation);
        }
    }

    private static void setMaterialWallTile(
            MapMaterial material,
            Schema.MaterialTiles.Material config,
            Schema.MaterialTiles.Size size,
            MapContentSurfaceDataSource<MapMaterial> dataSource,
            SpriteFactory spriteFactory,
            BufferedImage baseMaterialTile) throws IOException {

        SpriteSheet spriteSheet = spriteFactory.getSpriteSheet(config.wallTileSheet, size.width, size.height);

        MapWallTiles wallTiles = new MapWallTiles(spriteSheet, config.u, config.v, size.width, size.height, baseMaterialTile);

        dataSource.setWallTilesForMapTile(material, wallTiles);
    }

    private static void setMaterialBoundaryTile(
            MapMaterial material,
            Schema.MaterialTiles.Material config,
            Schema.MaterialTiles.Size size,
            MapContentSurfaceDataSource<MapMaterial> dataSource,
            SpriteFactory spriteFactory,
            BufferedImage baseMaterialTile) throws IOException {

        SpriteSheet spriteSheet = spriteFactory.getSpriteSheet(config.boundaryTileSheet, size.width, size.height);

        MapBoundaryTiles boundaryTiles = new MapBoundaryTiles(spriteSheet, config.u, config.v, size.width, size.height, baseMaterialTile);

        dataSource.setBoundaryTilesForMapTile(material, boundaryTiles);
    }

    private static Schema loadJson(String fileName) throws IOException {
        Logger.getGlobal().log(Level.INFO, "Loading material shapes from [{0}]", fileName);

        File jsonFile = new File(fileName);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Schema value = mapper.readValue(jsonFile, Schema.class);
        return value;
    }

}
