/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.tiles;

import com.mush.game.utils.sprites.SpriteAnimation;

/**
 *
 * @author mush
 */
public interface SurfaceDataSource {

    public SpriteAnimation getSurfaceData(int u, int v);
}
