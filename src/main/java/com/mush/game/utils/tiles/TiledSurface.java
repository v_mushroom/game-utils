/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.tiles;

import com.mush.game.utils.sprites.Sprite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mush
 */
public class TiledSurface {

    private int tileWidth;
    private int tileHeight;
    private int surfaceWidth;
    private int surfaceHeight;
    private int u0;
    private int v0;
    private int uWidth;
    private int vHeight;
    private double xOffset;
    private double yOffset;
    private final Point2D.Double position = new Point2D.Double();

    private Sprite[] tileSprites;
    private SurfaceDataSource dataSource;
    private boolean drawFrame = false;
    private boolean smoothTranslate = true;

    public TiledSurface(int width, int height, int tileWidth, int tileHeight) {
        this.surfaceWidth = width;
        this.surfaceHeight = height;
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
        createTiles();
        xOffset = 0;
        yOffset = 0;
    }

    public void resize(int width, int height) {
        clearTiles();
        this.surfaceWidth = width;
        this.surfaceHeight = height;
        createTiles();

        if (dataSource != null) {
            setDataSource(dataSource);
        }
    }

    private void clearTiles() {
        for (int i = 0; i < tileSprites.length; i++) {
            tileSprites[i] = null;
        }
        tileSprites = null;
    }

    private void createTiles() {
        uWidth = (int) Math.ceil((double) surfaceWidth / tileWidth);
        vHeight = (int) Math.ceil((double) surfaceHeight / tileHeight);
        u0 = 0;
        v0 = 0;
        int tilesWidth = uWidth + 1;
        int tilesHeight = vHeight + 1;
        int ind = 0;
        tileSprites = new Sprite[tilesWidth * tilesHeight];
        for (int i = 0; i < tilesWidth; i++) {
            for (int j = 0; j < tilesHeight; j++) {
                tileSprites[ind] = new Sprite(null);
                ind++;
            }
        }
    }

    public void setDataSource(SurfaceDataSource dataSource) {
        this.dataSource = dataSource;
        query(0, 0, uWidth + 1, vHeight + 1);
    }
    
    public void setSmoothTranslate(boolean value) {
        smoothTranslate = value;
    }

    public void setPosition(double x, double y) {
        xOffset = x % tileWidth;
        yOffset = y % tileHeight;
        u0 = (int) ((xOffset - x) / tileWidth);
        v0 = (int) ((yOffset - y) / tileHeight);
        query(0, 0, uWidth + 1, vHeight + 1);
    }

    public Point2D.Double getPosition() {
        position.x = -u0 * tileWidth + xOffset;
        position.y = -v0 * tileHeight + yOffset;
        return position;
    }

    public void update(double elapsedSeconds) {
        for (Sprite sprite : tileSprites) {
            sprite.update(elapsedSeconds);
        }
    }

    public void setDrawFrame(boolean drawFrame) {
        this.drawFrame = drawFrame;
    }

    public void draw(Graphics2D g) {
        g.setColor(Color.YELLOW);
        AffineTransform t = g.getTransform();
        if (smoothTranslate) {
            g.translate(xOffset, yOffset);
        } else {
            g.translate((int) xOffset, (int) yOffset);
        }

        int ind = 0;
        for (int j = 0; j < vHeight + 1; j++) {
            for (int i = 0; i < uWidth + 1; i++) {
                int x = i * tileWidth;
                int y = j * tileHeight;
                tileSprites[ind].draw(g, x, y);
//                drawTileFrame(g, x, y, i, j);
                ind++;
            }
        }
        g.setTransform(t);

        if (drawFrame) {
            g.setColor(Color.RED);
//        g.drawRect(5, 5, surfaceWidth - 10, surfaceHeight - 10);
            g.drawRect(0, 0, surfaceWidth, surfaceHeight);
        }
    }

    private void drawTileFrame(Graphics2D g, int x, int y, int i, int j) {
        int u = u0 + i;
        int v = v0 + j;
        g.drawRect(x, y, tileWidth, tileHeight);
        g.drawString(u + "," + v, x + 2, y + 12);
    }

    public void move(double dx, double dy) {
        xOffset += dx;
        yOffset += dy;

        if (xOffset + (uWidth + 1) * tileWidth < surfaceWidth) {
            xOffset += tileWidth;
            u0 += 1;
            shiftLeft();
            query(uWidth - 0, 0, 1, vHeight + 1);
        } else if (xOffset > 0) {
            xOffset -= tileWidth;
            u0 -= 1;
            shiftRight();
            query(0, 0, 1, vHeight + 1);
        }

        if (yOffset + (vHeight + 1) * tileHeight < surfaceHeight) {
            yOffset += tileHeight;
            v0 += 1;
            shiftUp();
            query(0, vHeight - 0, uWidth + 1, 1);
        } else if (yOffset > 0) {
            yOffset -= tileHeight;
            v0 -= 1;
            shiftDown();
            query(0, 0, uWidth + 1, 1);
        }
    }

    private void shiftLeft() {
        for (int v = 0; v < vHeight + 1; v++) {
            shift(v * (uWidth + 1), 1, uWidth);
        }
    }

    private void shiftRight() {
        for (int v = 0; v < vHeight + 1; v++) {
            shift(v * (uWidth + 1) + uWidth, -1, uWidth);
        }
    }

    private void shiftUp() {
        for (int u = 0; u < uWidth + 1; u++) {
            shift(u, uWidth + 1, vHeight);
        }
    }

    private void shiftDown() {
        int step = uWidth + 1;
        for (int u = 0; u < uWidth + 1; u++) {
            shift(u + step * vHeight, -step, vHeight);
        }
    }

    private void shift(int index0, int step, int steps) {
        int index = index0;
        Sprite first = tileSprites[index];
        for (int i = 0; i < steps; i++) {
            tileSprites[index] = tileSprites[index + step];
            index += step;
        }
        tileSprites[index] = first;
    }

    private void query(int u, int v, int width, int height) {
        if (dataSource == null) {
            return;
        }
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                query(u + i, v + j);
            }
        }
    }

    private void query(int u, int v) {
        int worldU = u + u0;
        int worldV = v + v0;
        int ind = u + v * (uWidth + 1);
        if (ind >= 0 && ind < tileSprites.length) {
            Sprite sprite = tileSprites[ind];
            sprite.setAnimation(dataSource.getSurfaceData(worldU, worldV));
            Double duration = sprite.getAnimationDuration();
            if (duration != null) {
                sprite.update(Math.random() * duration);
            }
        } else {
            Logger.getGlobal().log(Level.WARNING, "TiledSurface.query outside of bounds: {0}, {1}", new Object[]{u, v});
        }
    }

}
