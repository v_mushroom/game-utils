/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.tiles;

import com.mush.game.utils.map.MapBoundary;
import com.mush.game.utils.map.MapContent;
import com.mush.game.utils.map.MapContentComparator;
import com.mush.game.utils.map.MapWall;
import com.mush.game.utils.sprites.SpriteAnimation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mush
 */
public class MapContentSurfaceDataSource<T> implements SurfaceDataSource {

    private MapContent<T> mapContent;
    private final MapContentComparator<T> mapContentComparator;
    private final Map<T, MapWallTiles> wallTypes;
    private final Map<T, MapBoundaryTiles> boundaryTypes;
    private final Map<T, SpriteAnimation> mapTypes;
    private final Map<T, List<SpriteAnimation>> randomMapTypes;
    public final int tileWidth;
    public final int tileHeight;

    public MapContentSurfaceDataSource(/*MapContent<T> mapContent*/int tileWidth, int tileHeight) {
//        this.mapContent = mapContent;
        this(tileWidth, tileHeight, new MapContentComparator<>(/*mapContent*/));
    }

    public MapContentSurfaceDataSource(int tileWidth, int tileHeight, MapContentComparator<T> mapContentComparator) {
        this.mapContentComparator = mapContentComparator;
        wallTypes = new HashMap<>();
        boundaryTypes = new HashMap<>();
        mapTypes = new HashMap<>();
        randomMapTypes = new HashMap<>();
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
    }

    @Override
    public SpriteAnimation getSurfaceData(int u, int v) {
        if (mapContent == null) {
            return null;
        }

        T mapItem = mapContent.getItemAt(u, v);

        if (isWall(mapItem)) {
            return getWallSurfaceData(mapItem, u, v);
        }

        if (isBoundary(mapItem)) {
            SpriteAnimation surface = getBoundarySurfaceData(mapItem, u, v);
            if (surface != null) {
                return surface;
            }
        }
        
        if (isRandom(mapItem)) {
            SpriteAnimation surface = getAnimationForRandomMapTile(mapItem);
            if (surface != null) {
                return surface;
            }
        }

        return getAnimationForMapTile(mapItem);
    }

    private SpriteAnimation getWallSurfaceData(T mapItem, int u, int v) {
        MapWall.TileType wallTileType = MapWall.evaluate(u, v, mapContentComparator);
        MapWallTiles mapWallTiles = getWallTilesForMapTile(mapItem);
        return mapWallTiles.getAnimationForType(wallTileType);
    }

    private SpriteAnimation getBoundarySurfaceData(T mapItem, int u, int v) {
        MapBoundary.TileType boundaryTileType = MapBoundary.evaluate(u, v, mapContentComparator);
        MapBoundaryTiles mapBoundaryTiles = getBoundaryTilesForMapTile(mapItem);
        return mapBoundaryTiles.getAnimationForType(boundaryTileType);
    }

    public void setMapContent(MapContent<T> mapContent) {
        this.mapContent = mapContent;
        this.mapContentComparator.setMapContent(mapContent);
    }

    public MapContentComparator<T> getMapContentComparator() {
        return mapContentComparator;
    }

    public void setAnimationForMapTile(T mapItem, SpriteAnimation animation) {
        mapTypes.put(mapItem, animation);
    }

    public SpriteAnimation getAnimationForMapTile(T mapItem) {
        return mapTypes.get(mapItem);
    }
    
    public void setAnimationForRandomMapTile(T mapItem, SpriteAnimation animation) {
        List<SpriteAnimation> list = randomMapTypes.get(mapItem);
        if (list == null) {
            list = new ArrayList<>();
            randomMapTypes.put(mapItem, list);
        }
        list.add(animation);
    }
    
    public SpriteAnimation getAnimationForRandomMapTile(T mapItem) {
        List<SpriteAnimation> list = randomMapTypes.get(mapItem);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get((int) (Math.random() * list.size()));
    }

    public void setWallTilesForMapTile(T mapItem, MapWallTiles mapWallTiles) {
        wallTypes.put(mapItem, mapWallTiles);
    }

    public MapWallTiles getWallTilesForMapTile(T mapItem) {
        return wallTypes.get(mapItem);
    }

    private boolean isWall(T mapItem) {
        return wallTypes.containsKey(mapItem);
    }

    public void setBoundaryTilesForMapTile(T mapItem, MapBoundaryTiles mapBoundaryTiles) {
        boundaryTypes.put(mapItem, mapBoundaryTiles);
    }

    public MapBoundaryTiles getBoundaryTilesForMapTile(T mapItem) {
        return boundaryTypes.get(mapItem);
    }

    private boolean isBoundary(T mapItem) {
        return boundaryTypes.containsKey(mapItem);
    }
    
    private boolean isRandom(T mapItem) {
        return randomMapTypes.containsKey(mapItem);
    }

}
