/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.tiles;

import com.mush.game.utils.map.MapWall;
import static com.mush.game.utils.map.MapWall.TileType.*;
import com.mush.game.utils.sprites.SpriteAnimation;
import com.mush.game.utils.sprites.SpriteSheet;
import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 *
 * @author mush
 */
public class MapWallTiles {

    /*
     * 0---
     * |/+\
     * |+++
     * |\+/
     */

    private SpriteAnimation[] tileAnimations;

    // used only during construction
    private int u0;
    private int v0;
    private int spriteWidth;
    private int spriteHeight;
    private BufferedImage baseTile;

    public MapWallTiles(SpriteSheet spriteSheet, int u0, int v0) {
        this(spriteSheet, u0, v0, spriteSheet.getDefaultSpriteWidth(), spriteSheet.getDefaultSpriteHeight(), null);
    }

    public MapWallTiles(SpriteSheet spriteSheet, int u0, int v0, int spriteWidth, int spriteHeight, BufferedImage baseTile) {
        this.u0 = u0;
        this.v0 = v0;
        this.spriteWidth = spriteWidth;
        this.spriteHeight = spriteHeight;
        this.baseTile = baseTile;

        loadTiles(spriteSheet);
        
        this.baseTile = null;
    }

    public SpriteAnimation getAnimationForType(MapWall.TileType tileType) {
        return tileAnimations[tileType.ordinal()];
    }

    private void loadTiles(SpriteSheet spriteSheet) {
        tileAnimations = new SpriteAnimation[MapWall.TileType.values().length];
        
        setTile(getTile(spriteSheet, 0, 0), SINGLE);
        
        setTile(getTile(spriteSheet, 1, 0), RIGHT);
        setTile(getTile(spriteSheet, 2, 0), LEFT_RIGHT);
        setTile(getTile(spriteSheet, 3, 0), LEFT);
        
        setTile(getTile(spriteSheet, 0, 1), DOWN);
        setTile(getTile(spriteSheet, 0, 2), UP_DOWN);
        setTile(getTile(spriteSheet, 0, 3), UP);
        
        setTile(getTile(spriteSheet, 1, 1), RIGHT_DOWN);
        setTile(getTile(spriteSheet, 2, 1), LEFT_RIGHT_DOWN);
        setTile(getTile(spriteSheet, 3, 1), DOWN_LEFT);
        setTile(getTile(spriteSheet, 1, 2), UP_DOWN_RIGHT);
        setTile(getTile(spriteSheet, 2, 2), UP_DOWN_LEFT_RIGHT);
        setTile(getTile(spriteSheet, 3, 2), UP_DOWN_LEFT);
        setTile(getTile(spriteSheet, 1, 3), UP_RIGHT);
        setTile(getTile(spriteSheet, 2, 3), LEFT_RIGHT_UP);
        setTile(getTile(spriteSheet, 3, 3), LEFT_UP);
    }
    
    private void setTile(SpriteAnimation animation, MapWall.TileType tileType) {
        tileAnimations[tileType.ordinal()] = animation;
    }

    private SpriteAnimation getTile(SpriteSheet spriteSheet, int uOfs, int vOfs) {
        BufferedImage spriteImage = spriteSheet.cutImage(u0 + uOfs, v0 + vOfs, spriteWidth, spriteHeight);
        
        if (baseTile != null) {
            Graphics2D g = (Graphics2D) spriteImage.getGraphics();
            g.setComposite(AlphaComposite.DstOver);
            g.drawImage(baseTile, null, 0, 0);
            g.dispose();
        }
        
        return new SpriteAnimation(spriteImage);
    }

}
