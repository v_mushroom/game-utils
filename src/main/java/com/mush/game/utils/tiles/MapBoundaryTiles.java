/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.tiles;

import com.mush.game.utils.map.MapBoundary;
import static com.mush.game.utils.map.MapBoundary.TileType.*;
import com.mush.game.utils.sprites.SpriteAnimation;
import com.mush.game.utils.sprites.SpriteSheet;
import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 *
 * @author mush
 */
public class MapBoundaryTiles {

    /*
     * /-\/\
     * |O|\/
     * \-/\/
     */
    private SpriteAnimation[] tileAnimations;

    // used only during construction
    private int u0;
    private int v0;
    private int spriteWidth;
    private int spriteHeight;
    
    private BufferedImage baseTile;

    public MapBoundaryTiles(SpriteSheet spriteSheet, int u0, int v0, int spriteWidth, int spriteHeight, BufferedImage baseTile) {
        this.u0 = u0;
        this.v0 = v0;
        this.spriteWidth = spriteWidth;
        this.spriteHeight = spriteHeight;
        this.baseTile = baseTile;

        loadTiles(spriteSheet);
        
        this.baseTile = null;
    }

    public SpriteAnimation getAnimationForType(MapBoundary.TileType tileType) {
        return tileAnimations[tileType.ordinal()];
    }

    private void loadTiles(SpriteSheet spriteSheet) {
        tileAnimations = new SpriteAnimation[MapBoundary.TileType.values().length];

        setTile(getTile(spriteSheet, 0, 0), TOP_LEFT);
        setTile(getTile(spriteSheet, 1, 0), TOP);
        setTile(getTile(spriteSheet, 2, 0), TOP_RIGHT);
        
        setTile(getTile(spriteSheet, 0, 1), LEFT);
        setTile(getTile(spriteSheet, 1, 1), SINGLE); // pick somewhere else?
//        setTile(getTile(spriteSheet, 1, 1), ALL);
        setTile(null, ALL);
        setTile(getTile(spriteSheet, 2, 1), RIGHT);
        
        setTile(getTile(spriteSheet, 0, 2), BOTTOM_LEFT);
        setTile(getTile(spriteSheet, 1, 2), BOTTOM);
        setTile(getTile(spriteSheet, 2, 2), BOTTOM_RIGHT);
        
        setTile(getTile(spriteSheet, 3, 0), INV_TOP_LEFT);
        setTile(getTile(spriteSheet, 4, 0), INV_TOP_RIGHT);
        setTile(getTile(spriteSheet, 3, 1), INV_BOTTOM_LEFT);
        setTile(getTile(spriteSheet, 4, 1), INV_BOTTOM_RIGHT);
        
        setTile(getTile(spriteSheet, 3, 2), INV_BOTTOM_RIGHT_TOP_LEFT);
        setTile(getTile(spriteSheet, 4, 2), INV_TOP_RIGHT_BOTTOM_LEFT);        
    }

    private void setTile(SpriteAnimation animation, MapBoundary.TileType tileType) {
        tileAnimations[tileType.ordinal()] = animation;
    }

    private SpriteAnimation getTile(SpriteSheet spriteSheet, int uOfs, int vOfs) {
        BufferedImage spriteImage = spriteSheet.cutImage(u0 + uOfs, v0 + vOfs, spriteWidth, spriteHeight);
        
        if (baseTile != null) {
            Graphics2D g = (Graphics2D) spriteImage.getGraphics();
            g.setComposite(AlphaComposite.DstOver);
            g.drawImage(baseTile, null, 0, 0);
            g.dispose();
        }
        
        return new SpriteAnimation(spriteImage);
    }

}
