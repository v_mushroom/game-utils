/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.example.components;

import com.mush.game.utils.map.MapContent;
import com.mush.game.utils.map.material.MapMaterial;
import com.mush.game.utils.map.material.MapMaterials;
import com.mush.game.utils.map.move.Box;
import com.mush.game.utils.map.move.MapContentCollision;
import com.mush.game.utils.map.move.MapContentItemDimension;
import com.mush.game.utils.map.move.MapContentObstacleEvaluator;

/**
 *
 * @author mush
 */
public class MoveExample {

    public Box box;
    public MapContentItemDimension itemDimension;
    public MapContentObstacleEvaluator<MapMaterial> obstacleEvaluator;
    public MapContentCollision<MapMaterial> contentCollision;

    public MoveExample(int width, int height, MapMaterials materials, MapContent<MapMaterial> mapContent) {
        final MapMaterial wall = materials.getMaterialByName("WALL");

        itemDimension = new MapContentItemDimension(width, height);

        obstacleEvaluator = (MapMaterial item) -> wall.equals(item);

        box = new Box(0, 0, 10, 10);
        
        contentCollision = new MapContentCollision<>(itemDimension, obstacleEvaluator);
        contentCollision.setMapContent(mapContent);
    }
}
