/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.example;

import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.fx.GameApplication;
import com.mush.game.utils.fx.GameKeyHandler;
import com.mush.game.utils.core.Updateable;
import com.mush.game.utils.render.DelegatingGameRenderer;
import com.mush.game.utils.render.GameRenderer;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import javafx.scene.input.KeyCode;

/**
 *
 * @author mush
 */
public class FxExample extends GameApplication {

    private Updateable updateable;
    private GameRenderer renderer;
    private GameKeyHandler keyHandler;
    private ViewExample example;

    public FxExample() {
        example = new ViewExample();
        updateable = example;
        renderer = new DelegatingGameRenderer(450, 300, example);
        renderer.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        renderer.showFps(true);

        GameEventQueue.global().addEventListener(example);

        keyHandler = new GameKeyHandler();
        keyHandler.bindActionKey(KeyCode.H, "hello!");
        keyHandler.bindStateKey(KeyCode.A, KeyEvent.VK_A);
        keyHandler.bindStateKey(KeyCode.D, KeyEvent.VK_D);
        keyHandler.bindStateKey(KeyCode.W, KeyEvent.VK_W);
        keyHandler.bindStateKey(KeyCode.S, KeyEvent.VK_S);
        keyHandler.bindStateKey(KeyCode.Q, KeyEvent.VK_Q);
        keyHandler.bindStateKey(KeyCode.E, KeyEvent.VK_E);
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public Updateable geUpdateable() {
        return example;
    }

    @Override
    public GameRenderer getGameRenderer() {
        return renderer;
    }

    @Override
    public GameKeyHandler getGameKeyHandler() {
        return keyHandler;
    }
}
