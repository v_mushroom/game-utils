/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.example.components;

import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.core.GameInputEvent;
import com.mush.game.utils.core.GameKeyboard;
import com.mush.game.utils.core.OnGameEvent;
import com.mush.game.utils.core.TwoInputValue;
import com.mush.game.utils.swing.GameKeyListener;
import java.awt.event.KeyEvent;

/**
 *
 * @author mush
 */
public class InputExample {

    public TwoInputValue frontBack = new TwoInputValue();
    public TwoInputValue leftRight = new TwoInputValue();
    public TwoInputValue turnLeftRight = new TwoInputValue();

    public GameKeyListener keyListener;

    public InputExample() {
        GameKeyboard gameKeyboard = new GameKeyboard();
        gameKeyboard.bindActionKey(KeyEvent.VK_H, "hello!");
        gameKeyboard.bindStateKey(KeyEvent.VK_A, KeyEvent.VK_A);
        gameKeyboard.bindStateKey(KeyEvent.VK_D, KeyEvent.VK_D);
        gameKeyboard.bindStateKey(KeyEvent.VK_W, KeyEvent.VK_W);
        gameKeyboard.bindStateKey(KeyEvent.VK_S, KeyEvent.VK_S);
        gameKeyboard.bindStateKey(KeyEvent.VK_Q, KeyEvent.VK_Q);
        gameKeyboard.bindStateKey(KeyEvent.VK_E, KeyEvent.VK_E);

        keyListener = new GameKeyListener();
        keyListener.addKeyboard(gameKeyboard);

        GameEventQueue.global().addEventListener(this);
    }

    @OnGameEvent
    public void onAction(GameInputEvent.State state) {
        if (state.message instanceof Integer) {
            switch ((Integer) state.message) {
                case KeyEvent.VK_W:
                    frontBack.setInputA(state.active ? 10 : 0);
                    break;
                case KeyEvent.VK_S:
                    frontBack.setInputB(state.active ? 10 : 0);
                    break;
                case KeyEvent.VK_A:
                    leftRight.setInputA(state.active ? 10 : 0);
                    break;
                case KeyEvent.VK_D:
                    leftRight.setInputB(state.active ? 10 : 0);
                    break;
                case KeyEvent.VK_Q:
                    turnLeftRight.setInputB(state.active ? 1 : 0);
                    break;
                case KeyEvent.VK_E:
                    turnLeftRight.setInputA(state.active ? 1 : 0);
                    break;
            }
        }
    }

}
