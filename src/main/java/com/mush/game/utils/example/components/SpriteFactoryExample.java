/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.example.components;

import com.mush.game.utils.sprites.SpriteAnimation;
import com.mush.game.utils.sprites.SpriteDefinition;
import com.mush.game.utils.sprites.SpriteFactory;
import com.mush.game.utils.sprites.SpriteSheet;
import java.io.IOException;

/**
 *
 * @author mush
 */
public class SpriteFactoryExample {

    public static SpriteFactory createManualSpriteFactory() {
        SpriteSheet spriteSheet;
        try {
            spriteSheet = new SpriteSheet("example-resources/frames.png", 32, 32);
        } catch (IOException e) {
            e.printStackTrace(System.out);
            return null;
        }

        SpriteAnimation animation = new SpriteAnimation();
        animation.addFrame(spriteSheet.cutImage(0, 0), 1.0);
        animation.addFrame(spriteSheet.cutImage(1, 0), 1.0);
        animation.addFrame(spriteSheet.cutImage(0, 1), 1.0);
        animation.addFrame(spriteSheet.cutImage(1, 1), 1.0);

        SpriteDefinition definition = new SpriteDefinition();
        definition.setAnimationForState("default", animation);

        SpriteFactory factory = new SpriteFactory();
        factory.addSpriteType("BOX", definition);

        return factory;
    }

    public static SpriteFactory loadSpriteFactory() {
        SpriteFactory factory = new SpriteFactory();
        try {
            factory.load("example-resources/sprites.json");
        } catch (IOException e) {
            e.printStackTrace(System.out);
            return null;
        }
        return factory;
    }
}
