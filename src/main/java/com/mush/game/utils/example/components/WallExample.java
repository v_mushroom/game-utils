/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.example.components;

import com.mush.game.utils.map.FixedSizeMapContent;
import com.mush.game.utils.map.material.loader.TextMapLoader;
import com.mush.game.utils.map.material.MapMaterial;
import com.mush.game.utils.map.material.MapMaterials;
import com.mush.game.utils.map.material.MapMaterialsLoader;
import com.mush.game.utils.map.material.loader.ImageMapLoader;
import com.mush.game.utils.sprites.SpriteAnimation;
import com.mush.game.utils.sprites.SpriteSheet;
import com.mush.game.utils.tiles.MapContentSurfaceDataSource;
import com.mush.game.utils.tiles.MapWallTiles;
import com.mush.game.utils.tiles.MaterialDataSourceLoader;
import com.mush.game.utils.tiles.TiledSurface;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mush
 */
public class WallExample {

    public TiledSurface surface;
    public MapMaterials materials;
    public FixedSizeMapContent<MapMaterial> materialMap;

    public WallExample() {
        try {
            materials = MapMaterialsLoader.loadFromFile("example-resources/materials.json");
            Logger.getGlobal().log(Level.INFO, "materials:{0}", materials.getMaterialNames());

//            materialMap = TextMapLoader.loadFromFile("example-resources/material-map.txt", materials);
            materialMap = ImageMapLoader.loadFromFile("example-resources/material-map.json", materials);

            List<String> lines = TextMapLoader.output(materialMap, materials);
            lines.forEach((line) -> {
                Logger.getGlobal().log(Level.INFO, "output: [{0}]", line);
            });

            MapContentSurfaceDataSource<MapMaterial> materialDataSource
            = MaterialDataSourceLoader.loadFromFile("example-resources/materials.json", materials);
            
            materialDataSource.setMapContent(materialMap);

            // TODO: get size from MapContentSurfaceDataSource
            surface = new TiledSurface(250, 150, 16, 16);
            surface.setDataSource(materialDataSource);

        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, "Error loading", e);
        }
    }

    private void manualCreateDataSource(
            MapContentSurfaceDataSource<MapMaterial> materialDataSource,
            MapMaterials materials
    ) {
        SpriteSheet spriteSheet;
        try {
            spriteSheet = new SpriteSheet("example-resources/wall.png", 16, 16);
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, "Error loading", e);
            return;
        }

        MapWallTiles wallTiles = new MapWallTiles(spriteSheet, 0, 0, 16, 16, null);

        materialDataSource.setWallTilesForMapTile(materials.getMaterialByName("WALL"), wallTiles);

        SpriteAnimation animation = new SpriteAnimation(spriteSheet.cutImage(4, 0));
        materialDataSource.setAnimationForMapTile(materials.getMaterialByName("GROUND"), animation);
    }

}
