/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.example.components;

import com.mush.game.utils.sprites.SpriteAnimation;
import com.mush.game.utils.sprites.SpriteSheet;
import com.mush.game.utils.tiles.SurfaceDataSource;
import com.mush.game.utils.tiles.TiledSurface;
import java.io.IOException;

/**
 *
 * @author mush
 */
public class TiledSurfaceExample {

    public TiledSurface surface;

    public TiledSurfaceExample() {
        surface = new TiledSurface(350, 200, 32, 32);
        surface.setDataSource(makeSurfaceDataSource());

    }

    private static SurfaceDataSource makeSurfaceDataSource() {
        SpriteSheet spriteSheet;
        try {
            spriteSheet = new SpriteSheet("example-resources/frames2.png", 32, 32);
        } catch (IOException e) {
            e.printStackTrace(System.out);
            return null;
        }

        return new SurfaceDataSource() {
            SpriteAnimation[] animations;

            {
                animations = new SpriteAnimation[4];
                animations[0] = makeAnimation(0, 0);
                animations[1] = makeAnimation(1, 0);
                animations[2] = makeAnimation(2, 0);
                animations[3] = makeAnimation(3, 0);
            }

            private SpriteAnimation makeAnimation(int u, int v) {
                SpriteAnimation animation = new SpriteAnimation();

                animation.addFrame(spriteSheet.cutImage(u, v), 0.5);
                //animation.addFrame(spriteSheet.cutImage(u, v + 1), 0.5);

                return animation;
            }

            @Override
            public SpriteAnimation getSurfaceData(int u, int v) {
                return animations[(int) (Math.random() * animations.length)];
            }
        };
    }

}
