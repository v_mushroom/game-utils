/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.example;

import com.mush.game.utils.example.components.WallExample;
import com.mush.game.utils.example.components.TiledSurfaceExample;
import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.core.OnGameEvent;
import com.mush.game.utils.core.GameInputEvent;
import com.mush.game.utils.physics.FreeBody;
import com.mush.game.utils.sprites.Sprite;
import com.mush.game.utils.sprites.SpriteFactory;
import com.mush.game.utils.swing.Game;
import com.mush.game.utils.render.GameRenderer;
import com.mush.game.utils.render.TrackingCamera;
import com.mush.game.utils.core.Updateable;
import com.mush.game.utils.example.components.InputExample;
import com.mush.game.utils.example.components.MoveExample;
import com.mush.game.utils.example.components.SpriteFactoryExample;
import com.mush.game.utils.map.material.MapMaterial;
import com.mush.game.utils.map.move.Box;
import com.mush.game.utils.map.move.MapContentCollision;
import com.mush.game.utils.render.DelegatingGameRenderer;
import com.mush.game.utils.render.Renderable;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mush
 */
public class ViewExample implements Updateable, Renderable {

    Sprite sprite;
    FreeBody freeBody;
    TrackingCamera camera;
    int lineCounter = 0;
    double lineValue = 0;
    final Point2D.Double localV = new Point2D.Double();
    double fps;
    double lastFps;
    TiledSurfaceExample surfaceExample;
    WallExample wallExample;
    InputExample inputExample;
    MoveExample moveExample;

    public ViewExample() {
        inputExample = new InputExample();

        SpriteFactory factory
                // = SpriteFactoryExample.createManualSpriteFactory();
                = SpriteFactoryExample.loadSpriteFactory();

        if (factory == null) {
            throw new RuntimeException();
        }

        sprite = factory.createSprite("BOX");
        sprite.setState("default");

        surfaceExample = new TiledSurfaceExample();
        wallExample = new WallExample();

        moveExample = new MoveExample(16, 16, wallExample.materials, wallExample.materialMap);

        camera = new TrackingCamera(450, 300);
        camera.setSpeed(1);

        freeBody = new FreeBody(1);
        freeBody.xPosition = 10;
        freeBody.yPosition = 100;
    }

    private void drawBox(Graphics2D g, Box box) {
        int x = (int) box.left;
        int y = (int) box.top;
        int w = (int) (box.right - box.left - 1);
        int h = (int) (box.bottom - box.top - 1);

        g.setColor(Color.YELLOW);
        g.fillRect(x, y, w, h);
        g.setColor(Color.RED);
        g.drawRect(x, y, w, h);
    }

    @Override
    public void renderContent(Graphics2D g, GameRenderer renderer) {
        g.setColor(Color.GRAY);
        g.fillRect(0, 0, 450, 300);
        sprite.draw(g, 10, 10);
        AffineTransform t = g.getTransform();
        g.translate(50, 50);
        surfaceExample.surface.draw(g);
        wallExample.surface.draw(g);

        drawBox(g, moveExample.box);

        Point2D.Double p = surfaceExample.surface.getPosition();
        g.drawOval((int) p.x - 5, (int) p.y - 5, 10, 10);
        g.setTransform(t);

        freeBody.getLocalVelocity(localV);

        g.translate(freeBody.xPosition - camera.getxOffset(), freeBody.yPosition - camera.getyOffset());
        g.rotate(freeBody.zRotation);
        sprite.draw(g, -16, -16);

        g.setColor(Color.GREEN);
        g.drawLine(0, 0, (int) localV.x, (int) localV.y);

        g.setTransform(t);

        g.setColor(Color.BLACK);
        g.drawLine(0, 100 + lineCounter, 100, 100 + lineCounter);
        g.setColor(Color.WHITE);
        g.drawLine(0, 100 + (int) lineValue, 100, 100 + (int) lineValue);

        g.setColor(Color.WHITE);
        g.drawLine(100 + lineCounter, 0, 100 + lineCounter, (int) fps - 30);
    }

    @Override
    public void update(double elapsedSeconds) {
        lineCounter++;
        lineCounter %= 100;

        lineValue += elapsedSeconds * (100.0);
        lineValue %= 100;

        sprite.update(elapsedSeconds);

        freeBody.applyLocalForce(-inputExample.leftRight.getOutput() * 10, -inputExample.frontBack.getOutput() * 10);
        freeBody.applyTorque(inputExample.turnLeftRight.getOutput() * 5);
        freeBody.update(elapsedSeconds);
        freeBody.clearForces();
        GameEventQueue.processQueue();

        camera.setTarget(freeBody.xPosition, freeBody.yPosition/*, freeBody.xVelocity, freeBody.yVelocity*/);
        camera.update(elapsedSeconds);

        Point2D.Double surfacePosition = surfaceExample.surface.getPosition();
        surfaceExample.surface.move(-camera.getxOffset() - surfacePosition.x, -camera.getyOffset() - surfacePosition.y);
        surfaceExample.surface.update(elapsedSeconds);

        wallExample.surface.update(elapsedSeconds);

        Point2D.Double movement = new Point2D.Double(-inputExample.leftRight.getOutput() * 0.1, -inputExample.frontBack.getOutput() * 0.1);
        moveExample.box.offsetBy(movement.x, movement.y);
        Set<MapContentCollision.MapItemCollision<MapMaterial>> collisions
                = moveExample.contentCollision.checkMapItemMovementCollisions(moveExample.box, movement);
        if (!collisions.isEmpty()) {
            Logger.getGlobal().log(Level.INFO, "collisions:");
            collisions.forEach((collision) -> {
                Logger.getGlobal().log(Level.INFO, "with: {0} at {1}, {2}", new Object[]{collision.mapItem.name, collision.u, collision.v});
            });
        }
    }

    @Override
    public void updateCurrentFps(double fps) {
        this.lastFps = this.fps;
        this.fps = fps;
    }

    @OnGameEvent
    public void onAction(GameInputEvent.Action action) {
        System.out.println(action.message);
        freeBody.stop();
    }

    public static void main(String[] args) {
        Game.setupLogging();

        ViewExample exampleGame = new ViewExample();

        GameEventQueue.global().addEventListener(exampleGame);

        DelegatingGameRenderer renderer = new DelegatingGameRenderer(450, 300, exampleGame);
        renderer.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

        Game game = new Game(exampleGame, renderer, exampleGame.inputExample.keyListener);
        game.frameTitle = "Example";
        game.preferredSize.setSize(450, 300);
        game.setFrameUndecorated(false);
        game.setPauseOnLoseFocus(true);
        game.setUseActiveGraphics(false);
        game.start();

        game.getRefreshThread().setTargetFps(60);
        game.getRefreshThread().setTimekeepingInsanity(true);
        renderer.showFps(true);
    }

}
