/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.ui;

import java.awt.geom.Rectangle2D;

/**
 *
 * @author mush
 */
public class UiButton {

    public String text;
    public Rectangle2D shape;
    public boolean selected;
    public Object action;
    public UiButtonRenderer renderer;

    public UiButton(String text, UiButtonRenderer renderer) {
        this.text = text;
        shape = new Rectangle2D.Double(0, 0, 10, 10);
        this.renderer = renderer;
    }

    public void setPosition(double x, double y) {
        shape.setFrame(x, y, shape.getWidth(), shape.getHeight());
    }

    public void setSize(double width, double height) {
        shape.setFrame(shape.getX(), shape.getY(), width, height);
    }

}
