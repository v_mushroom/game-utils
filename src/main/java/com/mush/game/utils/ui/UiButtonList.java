/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.ui;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mush
 */
public class UiButtonList {

    public final List<UiButton> list;
    public final Point2D.Double location;
    public final Point2D.Double direction;
    public double spacing = 0;
    public int selectedIndex = -1;
    public boolean loopSelection = false;

    public UiButtonList() {
        list = new ArrayList<>();
        location = new Point2D.Double(0, 0);
        direction = new Point2D.Double(1, 0);
    }

    public void draw(Graphics2D g) {
        for (UiButton button : list) {
            if (button.renderer != null) {
                button.renderer.draw(button, g);
            }
        }
    }

    public void layout() {
        double x = location.x;
        double y = location.y;
        for (UiButton button : list) {
            button.shape.setFrame(x, y, button.shape.getWidth(), button.shape.getHeight());
            x += direction.x * (button.shape.getWidth() + spacing);
            y += direction.y * (button.shape.getHeight() + spacing);
        }
    }

    public void clearSelection() {
        for (UiButton button : list) {
            button.selected = false;
        }
    }

    public UiButton getSelected() {
        if (list.isEmpty()) {
            return null;
        }
        if (selectedIndex < 0 || selectedIndex >= list.size()) {
            return null;
        }
        return list.get(selectedIndex);
    }

    public void select(int index) {
        clearSelection();
        selectedIndex = index;
        UiButton button = getSelected();
        if (button != null) {
            button.selected = true;
        }
    }

    public void selectNext() {
        if (list.isEmpty()) {
            select(0);
        } else if (loopSelection) {
            select((selectedIndex + 1) % list.size());
        } else if (selectedIndex < list.size() - 1) {
            select(selectedIndex + 1);
        }
    }

    public void selectPrevious() {
        if (list.isEmpty()) {
            select(0);
        } else if (loopSelection) {
            select((selectedIndex + list.size() - 1) % list.size());
        } else if (selectedIndex > 0) {
            select(selectedIndex - 1);
        }
    }
}
