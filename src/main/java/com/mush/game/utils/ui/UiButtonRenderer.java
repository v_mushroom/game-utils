/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.ui;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;

/**
 *
 * @author mush
 */
public class UiButtonRenderer {

    void draw(UiButton button, Graphics2D g) {
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect((int) button.shape.getX(), (int) button.shape.getY(), (int) button.shape.getWidth(), (int) button.shape.getHeight());
        if (button.selected) {
            g.setColor(Color.WHITE);
        } else {
            g.setColor(Color.BLACK);
        }
        g.drawRect((int) button.shape.getX(), (int) button.shape.getY(), (int) button.shape.getWidth(), (int) button.shape.getHeight());

        FontMetrics fm = g.getFontMetrics();
        int h = fm.getAscent();
        int w = fm.charsWidth(button.text.toCharArray(), 0, button.text.length());

        int x0 = (int) button.shape.getX();
        int y0 = (int) button.shape.getY();
        int xOff = (int) (button.shape.getWidth() / 2 - w / 2);
        int yOff = (int) (button.shape.getHeight() / 2 + h / 2);

        g.drawString(button.text, x0 + xOff, y0 + yOff);
    }

}
