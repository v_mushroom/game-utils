/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.physics;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mush
 */
public class BodyCollisionTest {

    FreeBody bodyA;
    FreeBody bodyB;
    BodyEdge edge;
    double distance;
    double elasticity;

    public BodyCollisionTest() {
    }

    @Before
    public void setup() {
        distance = 10.0;
        elasticity = 1.0;
        bodyA = new FreeBody(1);
        bodyB = new FreeBody(1);
        bodyA.xPosition = 0;
        bodyB.xPosition = 1;
        bodyA.xVelocity = 0;
        bodyB.xVelocity = 0;

        edge = new CircularBodyEdge(distance / 2);
    }

    /**
     * Test of collide method, of class FreeBody.
     */
    @Test
    public void testCollide() {
        collideX(1, 1, 1, 0);
        collideX(1, 1, 0, -1);
        collideX(1, 1, 2, 1);
        collideX(1, 1, 1, -1);

        collideX(1000, 1, 1, 0);
        collideX(1, 1000, 1, 0);
        collideX(1000, 1, 0, -1);
        collideX(1, 1000, 0, -1);

        collideY(1, 1, 1, 0);
        collideY(1, 1, 0, -1);
        collideY(1, 1, 2, 1);
        collideY(1, 1, 1, -1);

        collideY(1000, 1, 1, 0);
        collideY(1, 1000, 1, 0);
        collideY(1000, 1, 0, -1);
        collideY(1, 1000, 0, -1);

        collideXY(1, 1, 1, 1, 0, 1);

        elasticity = 0.75;
        collideX(1, 1, 1, 0);
    }

    private void collideX(double mA, double mB, double vA, double vB) {
        bodyA.xPosition = 0;
        bodyB.xPosition = 1;
        bodyA.yPosition = 0;
        bodyB.yPosition = 0;
        bodyA.xVelocity = vA;
        bodyB.xVelocity = vB;
        bodyA.yVelocity = 0;
        bodyB.yVelocity = 0;
        bodyA.setMass(mA, 1);
        bodyB.setMass(mB, 1);

        BodyCollision.collide(bodyA, bodyB, edge, edge, elasticity);

        System.out.println("collide (" + mA + " : " + mB + ") " + vA + " : " + vB + " -> " + bodyA.xVelocity + " : " + bodyB.xVelocity);
    }

    private void collideY(double mA, double mB, double vA, double vB) {
        bodyA.xPosition = 0;
        bodyB.xPosition = 0;
        bodyA.yPosition = 0;
        bodyB.yPosition = 1;
        bodyA.xVelocity = 0;
        bodyB.xVelocity = 0;
        bodyA.yVelocity = vA;
        bodyB.yVelocity = vB;
        bodyA.setMass(mA, 1);
        bodyB.setMass(mB, 1);

        BodyCollision.collide(bodyA, bodyB, edge, edge, elasticity);

        System.out.println("collide (" + mA + " : " + mB + ") " + vA + " : " + vB + " -> " + bodyA.yVelocity + " : " + bodyB.yVelocity);
    }

    private void collideXY(double mA, double mB, double vAx, double vAy, double vBx, double vBy) {
        bodyA.xPosition = 0;
        bodyB.xPosition = 1;
        bodyA.yPosition = 0;
        bodyB.yPosition = 0;
        bodyA.xVelocity = vAx;
        bodyA.yVelocity = vAy;
        bodyB.xVelocity = vBx;
        bodyB.yVelocity = vBy;
        bodyA.setMass(mA, 1);
        bodyB.setMass(mB, 1);

        BodyCollision.collide(bodyA, bodyB, edge, edge, elasticity);

        System.out.println("collide (" + mA + " : " + mB + ") "
                + vAx + "," + vAy + " : " + vBx + "," + vBy
                + " -> "
                + bodyA.xVelocity + "," + bodyA.yVelocity + " : " + bodyB.xVelocity + "," + bodyB.yVelocity);
    }

}
