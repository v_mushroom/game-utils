/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mush
 */
public class MapWallTest {

    private static final int[][] MAP_HOLLOW = new int[][]{
        new int[]{0, 0, 0, 0, 0},
        new int[]{0, 1, 1, 1, 0},
        new int[]{0, 1, 0, 1, 0},
        new int[]{0, 1, 1, 1, 0},
        new int[]{0, 0, 0, 0, 0}
    };

    private static final int[][] MAP_CROSS = new int[][]{
        new int[]{0, 0, 0, 0, 0},
        new int[]{0, 0, 1, 0, 0},
        new int[]{0, 1, 1, 1, 0},
        new int[]{0, 0, 1, 0, 0},
        new int[]{0, 0, 0, 0, 0}
    };

    private static final int[][] MAP_I = new int[][]{
        new int[]{0, 0, 0, 0, 0},
        new int[]{0, 1, 1, 1, 0},
        new int[]{0, 0, 1, 0, 0},
        new int[]{0, 1, 1, 1, 0},
        new int[]{0, 0, 0, 0, 0}
    };

    private static final int[][] MAP_H = new int[][]{
        new int[]{0, 0, 0, 0, 0},
        new int[]{0, 1, 0, 1, 0},
        new int[]{0, 1, 1, 1, 0},
        new int[]{0, 1, 0, 1, 0},
        new int[]{0, 0, 0, 0, 0}
    };

    public MapWallTest() {
    }

    /**
     * Test of evaluate method, of class MapWall.
     */
    @Test
    public void testEvaluate() {

        assertEquals(MapWall.TileType.UP_DOWN_LEFT_RIGHT, MapWall.evaluate(0, 0, getComparator(MAP_CROSS)));
        assertEquals(MapWall.TileType.LEFT, MapWall.evaluate(1, 0, getComparator(MAP_CROSS)));
        assertEquals(MapWall.TileType.RIGHT, MapWall.evaluate(-1, 0, getComparator(MAP_CROSS)));
        assertEquals(MapWall.TileType.UP, MapWall.evaluate(0, 1, getComparator(MAP_CROSS)));
        assertEquals(MapWall.TileType.DOWN, MapWall.evaluate(0, -1, getComparator(MAP_CROSS)));

        assertEquals(MapWall.TileType.UP_DOWN, MapWall.evaluate(1, 0, getComparator(MAP_HOLLOW)));
        assertEquals(MapWall.TileType.UP_DOWN, MapWall.evaluate(-1, 0, getComparator(MAP_HOLLOW)));
        assertEquals(MapWall.TileType.LEFT_RIGHT, MapWall.evaluate(0, 1, getComparator(MAP_HOLLOW)));
        assertEquals(MapWall.TileType.LEFT_RIGHT, MapWall.evaluate(0, -1, getComparator(MAP_HOLLOW)));

        assertEquals(MapWall.TileType.RIGHT_DOWN, MapWall.evaluate(-1, -1, getComparator(MAP_HOLLOW)));
        assertEquals(MapWall.TileType.DOWN_LEFT, MapWall.evaluate(1, -1, getComparator(MAP_HOLLOW)));
        assertEquals(MapWall.TileType.LEFT_UP, MapWall.evaluate(1, 1, getComparator(MAP_HOLLOW)));
        assertEquals(MapWall.TileType.UP_RIGHT, MapWall.evaluate(-1, 1, getComparator(MAP_HOLLOW)));

        assertEquals(MapWall.TileType.LEFT_RIGHT_DOWN, MapWall.evaluate(0, -1, getComparator(MAP_I)));
        assertEquals(MapWall.TileType.LEFT_RIGHT_UP, MapWall.evaluate(0, 1, getComparator(MAP_I)));

        assertEquals(MapWall.TileType.UP_DOWN_LEFT, MapWall.evaluate(1, 0, getComparator(MAP_H)));
        assertEquals(MapWall.TileType.UP_DOWN_RIGHT, MapWall.evaluate(-1, 0, getComparator(MAP_H)));
    }

    private MapTileComparator getComparator(int[][] map) {

        MapTileComparator comparator = new MapTileComparator() {
            @Override
            public boolean isSame(int u, int v, int uOfs, int vOfs) {
                // Evaluate from center of 5x5 map
                return map[v + 2][u + 2] == map[v + vOfs + 2][u + uOfs + 2];
            }
        };
        return comparator;
    }

}
