/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.map;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author mush
 */
public class MapBorderTest {

    private boolean[] values;
    private MapTileComparator comparator = new MapTileComparator() {
        @Override
        public boolean isSame(int u, int v, int uOfs, int vOfs) {
            boolean value = getValue(uOfs, vOfs);
            return value;
        }
    };

    public MapBorderTest() {
    }

    @Before
    public void before() {
        values = new boolean[]{
            true, true, true,
            true, true, true,
            true, true, true
        };
    }

    private boolean getValue(int u, int v) {
        int ind = 4;
        ind += u;
        ind += v * 3;
        return values[ind];
    }

    private void setValue(int u, int v, boolean value) {
        int ind = 4;
        ind += u;
        ind += v * 3;
        values[ind] = value;
    }

    @Test
    public void testGetTypeNoBorder() {
        MapBorder.Type type = MapBorder.getType(0, 0, comparator);

        assertEquals(null, type);
    }

    @Test
    public void testGetTypeAdjacent() {
        before();
        setValue(0, -1, false);

        assertEquals(MapBorder.Type.TOP, MapBorder.getType(0, 0, comparator));

        before();
        setValue(0, 1, false);

        assertEquals(MapBorder.Type.BOTTOM, MapBorder.getType(0, 0, comparator));

        before();
        setValue(-1, 0, false);

        assertEquals(MapBorder.Type.LEFT, MapBorder.getType(0, 0, comparator));

        before();
        setValue(1, 0, false);

        assertEquals(MapBorder.Type.RIGHT, MapBorder.getType(0, 0, comparator));
    }

    @Test
    public void testGetTypeDiagonal() {
        before();
        setValue(0, -1, false);
        setValue(1, 0, false);

        assertEquals(MapBorder.Type.TOP_RIGHT, MapBorder.getType(0, 0, comparator));

        before();
        setValue(0, 1, false);
        setValue(1, 0, false);

        assertEquals(MapBorder.Type.BOTTOM_RIGHT, MapBorder.getType(0, 0, comparator));

        before();
        setValue(0, 1, false);
        setValue(-1, 0, false);

        assertEquals(MapBorder.Type.BOTTOM_LEFT, MapBorder.getType(0, 0, comparator));

        before();
        setValue(-1, 0, false);
        setValue(0, -1, false);

        assertEquals(MapBorder.Type.TOP_LEFT, MapBorder.getType(0, 0, comparator));
    }

    @Test
    public void testGetTypeInverse() {
        before();
        setValue(1, -1, false);

        assertEquals(MapBorder.Type.INV_TOP_RIGHT, MapBorder.getType(0, 0, comparator));

        before();
        setValue(1, 1, false);

        assertEquals(MapBorder.Type.INV_BOTTOM_RIGHT, MapBorder.getType(0, 0, comparator));

        before();
        setValue(-1, 1, false);

        assertEquals(MapBorder.Type.INV_BOTTOM_LEFT, MapBorder.getType(0, 0, comparator));

        before();
        setValue(-1, -1, false);

        assertEquals(MapBorder.Type.INV_TOP_LEFT, MapBorder.getType(0, 0, comparator));
    }

    @Test
    public void testGetTypeInverseDouble() {
        before();
        setValue(1, -1, false);
        setValue(-1, 1, false);

        assertEquals(MapBorder.Type.INV_TOP_RIGHT_BOTTOM_LEFT, MapBorder.getType(0, 0, comparator));

        before();
        setValue(1, 1, false);
        setValue(-1, -1, false);

        assertEquals(MapBorder.Type.INV_BOTTOM_RIGHT_TOP_LEFT, MapBorder.getType(0, 0, comparator));
    }

}
